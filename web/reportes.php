<?php
	session_name("farmacia_dif");
	session_start();

	if ( !isset($_SESSION["usuario"]) )
	{
		header("Location: ../");
		exit;
	}

	$usuario_nombre = $_SESSION["usuario"]["nombre"];
	$permisos = $_SESSION["usuario"]["permisos"];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Farmacia DIF | Sistema de Inventario</title>
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/foundation.min.css" />
    <!-- <link rel="stylesheet" href="../css/foundation.calendar.css"> -->
    <link rel="stylesheet" href="../css/dataTables.foundation.css">
    <link rel="stylesheet" href="../css/datatables.tabletools.css">
    <link rel="stylesheet" href="../css/jquery-ui.min.css">
	<link rel="stylesheet" href="../css/jquery-ui.theme.min.css">
	<link rel="stylesheet" href="../css/tag-it.min.css">
    <!-- <link rel="stylesheet" href="../css/responsive-tables.css"> -->
    <style>
    	.ui-autocomplete
      	{
		    max-height: 200px;
		    overflow-y: auto;
		    overflow-x: hidden;
		}

		.ui-autocomplete-loading { background:url("../css/images/cargando.gif") no-repeat right center }
		
		* html .ui-autocomplete
		{
		    height: 100px;
		}

    	table.print
  		{
  			width: 100%;
  			border: none;
  			border-bottom: solid 1px #DDD;
  			margin-bottom: 0.5em;
  		}

  		table.print2
  		{
  			width: 100%;
  			border: none;
  			margin: 0;
  		}

  		table.print2 td
  		{
  			padding: 0;
  		}

  		table.print2 h1,
  		table.print2 h2,
  		table.print2 h3,
  		table.print2 h4,
  		table.print2 h5,
  		table.print2 h6
  		{
  			margin: 0;
  		}

  		.DTTT_PrintMessage p
  		{
  			margin-bottom: 0.5em;
  		}

    	@media print
      	{
      		@page
      		{
      			margin: 15px;
      		}

      		.DTTT_PrintMessage
      		{
      			margin: 10px 15px;
      		}

      		.DTTT_print_info
      		{
      			display: none;
      		}

      		.oprima-esc
      		{
      			display: none;
      		}

      		table.dataTable td, table.dataTable th
      		{
      			font-size: 10px !important;
      			border: 1px solid grey;
      		}
      	}
    </style>
    <script src="../js/vendor/modernizr.js"></script>
</head>
<body>
	<nav id="top-bar-principal" class="top-bar" data-topbar>
		<ul class="title-area">
			<li class="name">
				<h1><a href="#">Farmacia DIF</a></h1>
			</li>
			<!-- <small class="show-for-small-only"><?php //echo "Bienvenido $usuario_nombre"; ?></small>-->

			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<ul class="right">
				<li><a href="index.php">Medicamentos</a></li>
				<li><a href="recetas.php">Recetas</a></li>
				<li><a href="reportes.php">Reportes</a></li>
				<!-- <li class="has-dropdown">
					<a href="eventos.php">Eventos</a>
					<ul class="dropdown">
						<li><a class="evento" href="#">Crear Evento</a></li>
						<li><a class="evento" href="#">Editar Evento</a></li>
					</ul>
				</li> -->
				<li><a id="cerrar-sesion" href="#">Cerrar Sesión</a></li>
			</ul>

			<ul class="left hide-for-small-only">
				<li><a href="#"><?php echo "Bienvenido <b>$usuario_nombre</b>"; ?></a></li>
			</ul>
		</section>
	</nav>

	<header>
		<div class="row">
			<div class="large-12 medium-12 small-12 columns">
				<h2 style="margin-bottom:0;">Reportes</h2>
			</div>

			<div class="large-12 medium-12 small-12 columns">
				<p class="subheader">
					Primero, elige el <strong>Tipo de Reporte</strong> que desees generar.
				</p>
			</div>

			<br>

			<!-- <div class="large-12 columns">
				<label for="tipo-reporte">Tipo de Reporte</label>
				<select id="tipo-reporte">
					<option value="obtener-reporte-medicamentos">Entradas y Salidas de Medicamentos</option>
					<option value="obtener-reporte-medicos">Medicamento recetado por Médicos</option>
					<option value="obtener-reporte-trabajadores">Medicamento recetado a los Trabajadores</option>
					<option value="obtener-reporte-historial-medicamentos">Historial de Movimientos de los Medicamentos</option>
				</select>
			</div> -->

			<div class="large-12 columns">
				<div class="row">
					<div class="large-3 medium 3 small-2 columns">
						<input type="radio" id="reporte-por-secretaria" name="reporte-por" title="Reporte de Recetas - Secretaría" value="4" checked>
						<label for="reporte-por-secretaria">Secretaría</label>
					</div>

					<div class="large-3 medium 3 small-2 columns">
						<input type="radio" id="reporte-por-departamento" name="reporte-por" title="Reporte de Recetas - Departamento" value="5">
						<label for="reporte-por-departamento">Departamento</label>
					</div>

					<div class="large-3 medium 3 small-2 columns">
						<input type="radio" id="reporte-por-empresa" name="reporte-por" title="Reporte de Recetas - Empresa" value="6">
						<label for="reporte-por-empresa">Empresa</label>
					</div>

					<div class="large-3 medium 3 small-2 columns">
						<input type="radio" id="reporte-por-trabajador" name="reporte-por" title="Reporte de Recetas - Trabajador" value="7">
						<label for="reporte-por-trabajador">Trabajador</label>
					</div>

					<div class="large-3 medium 3 small-2 columns">
						<input type="radio" id="reporte-por-beneficiario" name="reporte-por" title="Reporte de Recetas - Beneficiario" value="8">
						<label for="reporte-por-beneficiario">Beneficiario</label>
					</div>

					<div class="large-3 medium 3 small-2 columns">
						<input type="radio" id="reporte-por-apoyo" name="reporte-por" title="Reporte de Recetas - Apoyo" value="9">
						<label for="reporte-por-apoyo">Apoyo</label>
					</div>

					<div class="large-3 medium 3 small-2 columns end">
						<input type="radio" id="reporte-por-medico" name="reporte-por" title="Reporte de Recetas - Médico" value="10">
						<label for="reporte-por-medico">Médico</label>
					</div>

					<div class="large-3 medium 3 small-2 columns end">
						<input type="radio" id="reporte-por-min-max" name="reporte-por" title="Reporte de Existencias Mínimas y Máximas" value="11">
						<label for="reporte-por-min-max">Mínimos y Máximos</label>
					</div>

					<div class="large-3 medium 3 small-2 columns end">
						<input type="radio" id="reporte-por-ajuste-inventario" name="reporte-por" title="Reporte para Ajuste de Inventario" value="12">
						<label for="reporte-por-ajuste-inventario">Ajuste de Inventario</label>
					</div>
				</div>
			</div>

			<hr>

			<div class="large-12 medium-12 small-12 columns">
				<p class="subheader">
					Despúes, escribe la <strong>Fecha Inicial</strong> y la <strong>Fecha de Termino</strong>.
				</p>
			</div>

			<div class="large-6 medium-6 small-12 columns">
				<label for="fecha-inicial">Fecha Inicial</label>
				<input id="fecha-inicial" type="text" class="fecha">
			</div>

			<div class="large-6 medium-6 small-12 columns">
				<label for="fecha-termino">Fecha de Termino</label>
				<input id="fecha-termino" type="text" class="fecha">
			</div>

			<hr>

			<div class="large-12 medium-12 small-12 columns">
				<p class="subheader">
					Puedes filtrar el resultado buscando y eligiendo valores especificos en el cuardo de texto (opcional).<br>
					Por último, da clic en el botón <strong>Generar Reporte</strong>.
				</p>
			</div>

			<div class="large-8 medium-8 small-12 columns">
				<label for="autocomplete-filtros">Filtros</label>
				<input id="autocomplete-filtros" type="text">
			</div>

			<div class="large-4 medium-4 small-12 columns">
				<a id="generar-reporte" href="#" class="small button expand">Generar Reporte</a>
			</div>
		</div>
	</header>

	<div class="row">
		<div class="large-12 columns">
			<hr>
		</div>
	</div>

	<div class="row">
		<div class="large-12 columns">
			<table id="dt-reporte" class="tdisplay compact" style="width: 100%;">
				<thead>
				</thead>

				<tfoot>
					<tr></tr>
				</tfoot>
			</table>
		</div>
	</div>

	<div id="cargando-modal" class="tiny reveal-modal" data-reveal aria-hidden="true" role="dialog">
		<p class="text-center">Cargando... <img src="../css/images/cargando.gif"></p>
	</div>

	<script src="../js/vendor/jquery.js"></script>
	<script src="../js/vendor/jquery-ui.min.js"></script>
	<script src="../js/vendor/jquery.mask.min.js"></script>
    <script src="../js/vendor/jquery.dataTables.min.js"></script>
	<script src="../js/vendor/dataTables.foundation.js"></script>
	<script src="../js/vendor/datatables.tabletools.js"></script>
	<script src="../js/vendor/tag-it.min.js"></script>
	<script src="../js/foundation.min.js"></script>
  	<script src="../js/foundation/foundation.topbar.js"></script>
  	<script src="../js/foundation/foundation.reveal.js"></script>
  	<script src="../js/foundation/foundation.tooltip.js"></script>
  	<script>$(document).foundation({
  		topbar :
  		{
			custom_back_text: false,
			is_hover: false,
			mobile_show_parent_link: false
		},
		reveal :
		{
			animation_speed: 0,
			close_on_background_click: false,
			multiple_opened: true
		}
  	});</script>
	<script>
		function lpad(n, width, z)
		{
			z = z || '0';
			n = n + '';
			return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
		};

		window.onload = function()
		{
			var topBar =
			{
				cerrarSesion : document.getElementById("cerrar-sesion")
			};
			var modal =
			{
				cargando : document.getElementById("cargando-modal")
			};

			topBar.cerrarSesion.onclick = function()
			{
				$.post( "../php/api.php",
				{
					accion: "cerrar-sesion",
				}, function( data )
				{
				  	if ( data.status === "OK" )
				  	{
				  		window.location.href = "../";
				  	}
				}, "json");
			};

			var initAutocompleteFiltros = function ()
			{
				/*$("#autocomplete-filtros").autocomplete(
				{
				    source: "../php/autocomplete.php?o="+$("input[name='reporte-por']:checked").val(),
				    minLength: 2,
				    select: function( event, ui )
				    {
				    	this.dataset.id = ui.item.id;
				    	//$("#autocomplete-medico-id").val(this.dataset.id);
				    	return;
				    },
				    change: function( event, ui )
				    {
				    	if (ui.item === null)
				    	{
				    		this.dataset.id = 0;
				    		$("#autocomplete-filtros").val("");
				    		//$("#autocomplete-medico-id").val(this.dataset.id);
				    	}
				    }
				});*/

				$("#autocomplete-filtros").tagit(
				{
					autocomplete:
					{
						source: "../php/autocomplete.php?o="+$("input[name='reporte-por']:checked").val(),
					    minLength: 0,
					    delay: 0
					},
					showAutocompleteOnFocus: true
				});
			};

			initAutocompleteFiltros();

			$("input[name='reporte-por']").change(function()
			{
				$("#autocomplete-filtros").tagit("removeAll");
				$("#autocomplete-filtros").tagit("destroy");
				initAutocompleteFiltros();

				switch (this.id)
				{
					case "reporte-por-min-max":
						$("#fecha-inicial").attr("disabled", true);
						$("#fecha-termino").attr("disabled", true);
						$("ul.tagit").hide();
					break;

					case "reporte-por-ajuste-inventario":
						$("#fecha-inicial").attr("disabled", true);
						$("#fecha-termino").attr("disabled", true);
						$("ul.tagit").hide();
					break;

					default:
						$("#fecha-inicial").attr("disabled", false);
						$("#fecha-termino").attr("disabled", false);
						$("ul.tagit").show();
					break;
				}
			});

			var date = new Date();
			var dia = lpad(date.getDate(), 2, "0");
			var mes = lpad(date.getMonth()+1, 2, "0");
			var temporada = date.getFullYear();
			var fecha = dia + "/" + mes + "/" + temporada;

			// Inicializar los input con Mask
			$(".fecha").mask("00/00/0000", {clearIfNotMatch: true} ).val(fecha);

			// Evento de Generar Reporte
			$("#generar-reporte").click(function()
			{
				var tipo_reporte = $("[name='reporte-por']:checked").get(0).id;
				var fecha_inicial = $("#fecha-inicial").val();
				var fecha_termino = $("#fecha-termino").val();

				if (fecha_inicial === "") { $("#fecha-inicial").focus(); return false; };
				if (fecha_termino === "") { $("#fecha-termino").focus(); return false; };

				// Tags
				var filtros = [];
				$("span.tagit-label").each(function()
				{
					filtros.push(this.textContent);
				});

				$(modal.cargando).foundation("reveal", "open");

				$.post( "../php/api.php",
				{
					accion: "obtener-reporte",
					tipo_reporte : tipo_reporte,
					fecha_inicial: fecha_inicial,
					fecha_termino: fecha_termino,
					//filtros: $("#autocomplete-filtros").val()
					filtros: JSON.stringify(filtros)
				}, function( data )
				{
				  	if ( data.status === "OK" )
				  	{
				  		if ($('#dt-reporte').hasClass('dataTable'))
				  		{
				  			$('#dt-reporte').DataTable().destroy();
				  			$('#dt-reporte').empty();
				  		}
				  		
				  		for (var i = 0; i < data.resultado.columns.length; i++)
				  		{
				  			$('#dt-reporte thead tr').append("<th>"+data.resultado.columns[i].title+"</th>");
				  		};

				  		// Inicializar Datatables
					    $('#dt-reporte').dataTable( {
					    	"language":
					    	{
								"url": "json/datatables.spanish.lang.json"
							},
							"columns": data.resultado.columns,
							"data": data.resultado.data,
							"pageLength": -1,
							"lengthMenu": [ [-1], ["Todos"] ],
							"dom": 'T<"clear">lfrtip',
					        "tableTools":
					        {
					            "sSwfPath": "swf/copy_csv_xls_pdf.swf",
					            "aButtons": [
					            	// {
					            	// 	"sExtends": "copy",
					            	// 	"sButtonText": "Copiar",
					            	// 	"oSelectorOpts":
					            	// 	{
							           //      filter: "applied"
							           //  }
					            	// },
					            	{
					            		"sExtends": "print",
					            		"sButtonText": "Imprimir",
					            		"sInfo": "Para imprimir presione<h6>CTRL + P</h6>Para regresar presione la tecla ESC.",
					            		"sMessage": "<table class=print><tr><td><img src='../css/images/dif-logo.png' style='width:80px'></td><td><table class='print2'><tr><td><h2 class=text-center>DIF FARMACIA</h2></td></tr><tr><td><span id='fecha-hoy' class='text-center' style='display:block'>"+dia+"/"+mes+"/"+temporada+"</span>"+
					            			"<span id='rango-fechas-reporte' class='text-center' style='display:block;'>Del "+$("#fecha-inicial").val()+" al "+$("#fecha-termino").val()+"</span><h4 class=text-center>"+$("[name='reporte-por']:checked").attr("title")+"</h4></td></tr></table></td><td class=text-right><img src='../css/images/altamira-logo.png' style='width:80px'></td></tr></table>"+
					            			"<span class='oprima-esc' style='  float: right;font-weight: bold;'>Oprima la tecla ESC para regresar.</span>",
					            		"fnComplete": function ( nButton, oConfig, oFlash, sFlash ) {
					                        if ($("#reporte-por-min-max").is(":checked") || $("#reporte-por-ajuste-inventario").is(":checked")) {
					                        	$("#rango-fechas-reporte").hide();
					                        	$("#fecha-hoy").show();
					                        } else {
					                        	$("#rango-fechas-reporte").show();
					                        	$("#fecha-hoy").hide();
					                        }
					                    }
					            	}
					            	// {
					            	// 	"sExtends": "pdf",
					            	// 	"sTitle": "<table class=print><tr><td><img src='../css/images/dif-logo.png' style='width:80px'></td><td><h2 class=text-center>DIF FARMACIA</h2></td><td class=text-right><img src='../css/images/altamira-logo.png' style='width:80px'></td></tr></table><p class=text-center>"+$("#tipo-reporte option:selected").text().toUpperCase()+
					            	// 		" DEL <strong>"+$("#fecha-inicial").val()+"</strong> AL <strong>"+$("#fecha-termino").val()+"</strong></p>",
					            	// 	"oSelectorOpts":
					            	// 	{
							           //      filter: "applied"
							           //  }
					            	// },
					            	// {
					            	// 	"sExtends": "xls",
					            	// 	"sButtonText": "Excel",
					            	// 	"oSelectorOpts":
					            	// 	{
							           //      filter: "applied"
							           //  }
					            	// }
					            ]
					        },
					        "createdRow": function( row, data, dataIndex )
					        {
					        	row.cells[0].textContent = dataIndex+1;
					        },
					        // "footerCallback": function( row, data, start, end, display )
					        // {
					        //     var api = this.api(), data;
					        //     if (display.length === 0) { $(row).html(""); return; };
					 
					        //     // Remove the formatting to get integer data for summation
					        //     var intVal = function ( i )
					        //     {
					        //         return typeof i === 'string' ?
					        //             i.replace(/[\$,]/g, '')*1 :
					        //             typeof i === 'number' ?
					        //                 i : 0;
					        //     };

					        //     var tdTarget = $(row).find("td.cantidad").length === 0 ? ":eq(0)" : ".td-cantidad";

					        //     var total = api.column( tdTarget, { filter : "applied"} ).data().reduce( function (a, b)
					        //     {
				         //            return intVal(a) + intVal(b);
				         //        });

					        //     //var firstColspan = api.column( ".td-cantidad" ).index();
					        //     //var lastColspan = $(row).closest("table").find("thead tr").children().length - (api.column( ".td-cantidad" ).index() + 1);
					        //     var firstColspan = api.column( tdTarget ).index();
					        //     var lastColspan = $(row).closest("table").find("thead tr").children().length - (api.column( tdTarget ).index() + 1);
				         //        $(row).html("<th colspan="+firstColspan+" class=text-center>TOTAL</th><th>"+total+"</th><th colspan="+lastColspan+"></th>");
					        // },
					        "initComplete": function(settings, json)
					        {
					        	$("#ToolTables_dt-reporte_0").click();
					        },
							"destroy": true,
							"deferRender": true
					    });
				  	}

				  	$(modal.cargando).foundation("reveal", "close");
				}, "json");
			});

			var callMinMax = "<?php echo isset($_GET['minMax']) ? 'YES' : 'NO'; ?>";
			if (callMinMax === "YES")
			{
				$("#reporte-por-min-max").attr("checked", true);
				$("#generar-reporte").click();
			}
		};
	</script>
</body>
</html>