<?php
	session_name("farmacia_dif");
	session_start();

	if ( !isset($_SESSION["usuario"]) )
	{
		header("Location: ../");
		exit;
	}

	$usuario_nombre = $_SESSION["usuario"]["nombre"];
	$permisos = $_SESSION["usuario"]["permisos"];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Farmacia DIF | Sistema de Inventario</title>
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/foundation.min.css" />
    <!-- <link rel="stylesheet" href="../css/foundation.calendar.css"> -->
    <link rel="stylesheet" href="../css/dataTables.foundation.css">
    <link rel="stylesheet" href="../css/jquery-ui.min.css">
	<link rel="stylesheet" href="../css/jquery-ui.theme.min.css">
    <!-- <link rel="stylesheet" href="../css/responsive-tables.css"> -->
    <style>
    	.ui-autocomplete
      	{
		    max-height: 200px;
		    overflow-y: auto;
		    overflow-x: hidden;
		}

		.ui-autocomplete-loading { background:url("../css/images/cargando.gif") no-repeat right center }
		
		* html .ui-autocomplete
		{
		    height: 100px;
		}
    </style>
    <script src="../js/vendor/modernizr.js"></script>
</head>
<body>
	<nav id="top-bar-principal" class="top-bar" data-topbar>
		<ul class="title-area">
			<li class="name">
				<h1><a href="#">DIF Farmacia</a></h1>
			</li>
			<!-- <small class="show-for-small-only"><?php //echo "Bienvenido $usuario_nombre"; ?></small>-->

			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<ul class="right">
				<li><a href="index.php">Medicamentos</a></li>
				<li><a href="recetas.php">Recetas</a></li>
				<!-- <li><a href="reportes.php">Reportes</a></li> -->
				<!-- <li class="has-dropdown">
					<a href="eventos.php">Eventos</a>
					<ul class="dropdown">
						<li><a class="evento" href="#">Crear Evento</a></li>
						<li><a class="evento" href="#">Editar Evento</a></li>
					</ul>
				</li> -->
				<li><a id="cerrar-sesion" href="#">Cerrar Sesión</a></li>
			</ul>

			<ul class="left hide-for-small-only">
				<li><a href="#"><?php echo "Bienvenido <b>$usuario_nombre</b>"; ?></a></li>
			</ul>
		</section>
	</nav>

	<header>
		<div class="row">
			<div class="large-12 medium-12 small-12 columns">
				<h2 style="margin-bottom:0;">Recetas</h2>
			</div>

			<?php if(isset($_GET["e"]))
				{
					if ($_GET["e"] === "-2")
					{
						echo "<div class='large-12 columns'><div data-alert class='alert-box success'>¡El <strong>estatus de la receta</strong> fue <strong>cambiado</strong> con éxito!<a href='#' class='close'>&times;</a></div></div>";
					}
					else if ($_GET["e"] === "-1")
					{
						echo "<div class='large-12 columns'><div data-alert class='alert-box success'>¡La receta fue <strong>editada</strong> con éxito!<a href='#' class='close'>&times;</a></div></div>";
					}
					else if ($_GET["e"] === "0")
					{
						echo "<div class='large-12 columns'><div data-alert class='alert-box success'>¡La receta fue <strong>elaborada</strong> con éxito!<a href='#' class='close'>&times;</a></div></div>";
					}
					else if ($_GET["e"] === "1")
					{
						echo "<div class='large-12 columns'><small class='error'>¡Oops! Algo salió mal. Favor de intentar hacer la operación de nuevo.</small></div>";
					}
					else if ($_GET["e"] === "2")
					{
						echo "<div class='large-12 columns'><small class='error'>Se elaboro la receta con éxito, pero los medicamentos no fueron añadidos correctamente.<br><strong>Por favor, busca la receta en la tabla inferior, da clic en su botón de 'Editar' e intente añadir nuevamente los medicamentos.</strong></small></div>";
					}
					else if ($_GET["e"] === "3")
					{
						echo "<div class='large-12 columns'><small class='error'>¡Oh no!, la cantidad ingresada es <strong>mayor</strong> a la cantidad disponible actual del medicamento. Favor de revisar.</small></div>";
					}
				}
			?>

			<div class="large-8 medium-8 small-12 columns">
				<p class="subheader">Da clic en <strong>Elaborar Receta</strong> para ingresar manualmente la información de una receta médica.</p>
			</div>

			<div class="large-4 medium-4 small-12 columns">
				<a id="elaborar-receta" href="#" class="small button expand">Elaborar Receta</a>
			</div>
		</div>
	</header>

	<div class="row">
		<div class="large-12 columns">
			<hr>
		</div>
	</div>

	<div class="row">
		<div class="large-12 columns">
			<table id="dt-recetas" class="tdisplay compact" style="width: 100%;">
				<thead>
					<th>#</th>
					<th>Folio</th>
					<th>Médico</th>
					<th>Trabajador</th>
					<th>Empresa</th>
					<th>Estatus</th>
					<th></th>
				</thead>
			</table>
		</div>
	</div>

	<div id="receta-modal" class="reveal-modal" data-reveal aria-labelledby="receta-modal-titulo" aria-hidden="true" role="dialog">
	  	<h2 id="receta-modal-titulo">Elaborar Receta</h2>
	  	<div class="row">
	  		<div class="large-12 columns">
	  			<form id="receta-form" action="../php/api.php" method="POST">
	  				<div class="row">
	  					<div class="large-12 columns">
	  						<label for="fechahora-elaboracion"><span data-tooltip aria-haspopup="true" class="has-tip" title="Escribe la fecha y hora de la receta (dd/mm/aaaa hh:mm).">Fecha y Hora de la Receta*</span></label>
	  						<input id="fechahora-elaboracion" name="fechahora-elaboracion" type="text">
	  					</div>

	  					<div class="large-12 columns">
	  						<label for="folio"><span data-tooltip aria-haspopup="true" class="has-tip" title="Escribe el folio de la receta.">Folio*</span></label>
	  						<input id="folio" name="folio" type="text">
	  					</div>

	  					<div class="large-12 columns">
	  						<label for="autocomplete-medico"><span data-tooltip aria-haspopup="true" class="has-tip" title="Empieza escribiendo el nombre del médico y elígelo de las opciones resultantes.">Médico*</span></label>
	  						<input id="autocomplete-medico" name="medico" type="text" required>
	  						<input id="autocomplete-medico-id" type="hidden" name="id-medico" value="">
	  					</div>

	  					<div class="large-12 columns">
	  						<label for="autocomplete-trabajador">
	  							<span data-tooltip aria-haspopup="true" class="has-tip" title="Empieza escribiendo el nombre del trabajador y elígelo de las opciones resultantes.">Trabajador*</span>
	  						</label>
	  						<input id="autocomplete-trabajador" name="trabajador" type="text" required>
	  						<input id="autocomplete-trabajador-id" type="hidden" name="id-trabajador" value="">
	  					</div>

	  					<div class="large-12 columns">
	  						<label for="beneficiarios">
	  							<span data-tooltip aria-haspopup="true" class="has-tip" title="La lista muestra los beneficiarios actuales del trabajador elegido.">Beneficiario*</span>
	  						</label>
	  						<select name="beneficiario" id="beneficiarios"></select>
	  					</div>

	  					<div class="large-12 medium-12 small-12 columns">
	  						<label for="medicamento"><span data-tooltip aria-haspopup="true" class="has-tip" title="Empieza escribiendo el nombre del medicamento y elígelo de las opciones resultantes.">Medicamento*</span></label>
	  						<input id="autocomplete-medicamento" type="text">
	  					</div>

	  					<!-- <div class="large-4 medium-4 small-12 columns">
	  						<br>
	  						<input id="agregar-medicamento" type="button" class="small button expand" value="Agregar" disabled>
	  					</div> -->

	  					<div class="large-12 columns">
	  						<table id="table-medicamentos-receta" style="width:100%;">
	  							<thead>
	  								<th>#</th>
	  								<th>Medicamento</th>
	  								<th>Cantidad Disponible</th>
	  								<th>Cantidad</th>
	  								<th></th>
	  							</thead>

	  							<tbody></tbody>
	  						</table>
	  					</div>

	  					<div class="large-12 columns">
							<input id="receta-completa" name="receta-completa" type="checkbox" style="width:24px;height:24px;vertical-align: top;" onchange="var t=this.parentNode.querySelector('#text-estatus-receta');t.textContent=!this.checked?'PENDIENTE':'COMPLETA';" checked>
							<label for="receta-completa">Esta receta se encuentra <strong id="text-estatus-receta">COMPLETA</strong>.</label>
	  					</div>

	  					<div class="large-offset-8 large-4 medium-offset-8 medium-4 small-12 columns end">
	  						<small>* Campos obligatorios.</small>
	  						<input id="receta-enviar" type="submit" class="small button expand" value="Enviar">
	  						<input id="receta-accion" type="hidden" name="accion" value="elaborar-receta">
	  						<input id="receta-id" type="hidden" name="id">
	  					</div>
	  				</div>
	  			</form>
	  		</div>
	  	</div>
	  	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	</div>

	<div id="ver-mas-receta-modal" class="reveal-modal" data-reveal aria-labelledby="ver-mas-receta-modal-titulo" aria-hidden="true" role="dialog">
	  	<h2 id="ver-mas-receta-modal-titulo">Ver más información</h2>
	  	<div class="row">
	  		<h4>Receta</h4>
	  		<div class="large-12 columns panel">
	  			<div class="large-2 medium-2 small-12 columns">
	  				<label><strong>Elaboración</strong></label><span id='ver-mas-receta-fechahora' class='ver-mas-receta-info'></span>
	  			</div>
	  			<div class="large-2 medium-2 small-12 columns">
	  				<label><strong>Folio</strong></label><span id='ver-mas-receta-folio' class='ver-mas-receta-info'></span>
	  			</div>
	  			<div class="large-3 medium-3 small-12 columns">
	  				<label><strong>Médico</strong></label><span id='ver-mas-receta-medico' class='ver-mas-receta-info'></span>
	  			</div>
	  			<div class="large-3 medium-3 small-12 columns">
	  				<label><strong>Trabajador</strong></label><span id='ver-mas-receta-trabajador' class='ver-mas-receta-info'></span>
	  				(<strong id="ver-mas-receta-empresa" style="font-size:10px;"></strong>)
	  			</div>
	  			<div class="large-2 medium-2 small-12 columns">
	  				<label><strong>Estatus</strong></label><span id='ver-mas-receta-estatus' class='ver-mas-receta-info'></span>
	  			</div>
	  		</div>

	  		<hr>

	  		<div class="large-12 columns">
	  			<h4>Medicamentos</h4>
	  			<table id="table-medicamentos-ver-mas-receta" class="tdisplay compact" style="width: 100%;">
					<thead>
						<th>#</th>
						<th>Medicamento</th>
						<!-- <th>Cantidad Disponible</th> -->
						<th>Cantidad</th>
					</thead>

					<tbody></tbody>
				</table>
	  		</div>
	  	</div>
	  	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	</div>

	<div id="cargando-modal" class="tiny reveal-modal" data-reveal aria-hidden="true" role="dialog">
		<p class="text-center">Cargando... <img src="../css/images/cargando.gif"></p>
	</div>

	<script src="../js/vendor/jquery.js"></script>
	<script src="../js/vendor/jquery-ui.min.js"></script>
	<script src="../js/vendor/jquery.mask.min.js"></script>
    <script src="../js/vendor/jquery.dataTables.min.js"></script>
	<script src="../js/vendor/dataTables.foundation.js"></script>
	<script src="../js/foundation.min.js"></script>
  	<script src="../js/foundation/foundation.topbar.js"></script>
  	<script src="../js/foundation/foundation.reveal.js"></script>
  	<script src="../js/foundation/foundation.tooltip.js"></script>
  	<script>$(document).foundation({
  		topbar :
  		{
			custom_back_text: false,
			is_hover: false,
			mobile_show_parent_link: false
		},
		reveal :
		{
			animation_speed: 0,
			close_on_background_click: false,
			multiple_opened: true
		}
  	});</script>
	<script>
		//
		// Pipelining function for DataTables. To be used to the `ajax` option of DataTables
		//
		$.fn.dataTable.pipeline = function ( opts ) {
		    // Configuration options
		    var conf = $.extend( {
		        pages: 5,     // number of pages to cache
		        url: '',      // script url
		        data: null,   // function or object with parameters to send to the server
		                      // matching how `ajax.data` works in DataTables
		        method: 'GET' // Ajax HTTP method
		    }, opts );
		 
		    // Private variables for storing the cache
		    var cacheLower = -1;
		    var cacheUpper = null;
		    var cacheLastRequest = null;
		    var cacheLastJson = null;
		 
		    return function ( request, drawCallback, settings ) {
		        var ajax          = false;
		        var requestStart  = request.start;
		        var drawStart     = request.start;
		        var requestLength = request.length;
		        var requestEnd    = requestStart + requestLength;
		         
		        if ( settings.clearCache ) {
		            // API requested that the cache be cleared
		            ajax = true;
		            settings.clearCache = false;
		        }
		        else if ( cacheLower < 0 || requestStart < cacheLower || requestEnd > cacheUpper ) {
		            // outside cached data - need to make a request
		            ajax = true;
		        }
		        else if ( JSON.stringify( request.order )   !== JSON.stringify( cacheLastRequest.order ) ||
		                  JSON.stringify( request.columns ) !== JSON.stringify( cacheLastRequest.columns ) ||
		                  JSON.stringify( request.search )  !== JSON.stringify( cacheLastRequest.search )
		        ) {
		            // properties changed (ordering, columns, searching)
		            ajax = true;
		        }
		         
		        // Store the request for checking next time around
		        cacheLastRequest = $.extend( true, {}, request );
		 
		        if ( ajax ) {
		            // Need data from the server
		            if ( requestStart < cacheLower ) {
		                requestStart = requestStart - (requestLength*(conf.pages-1));
		 
		                if ( requestStart < 0 ) {
		                    requestStart = 0;
		                }
		            }
		             
		            cacheLower = requestStart;
		            cacheUpper = requestStart + (requestLength * conf.pages);
		 
		            request.start = requestStart;
		            request.length = requestLength*conf.pages;
		 
		            // Provide the same `data` options as DataTables.
		            if ( $.isFunction ( conf.data ) ) {
		                // As a function it is executed with the data object as an arg
		                // for manipulation. If an object is returned, it is used as the
		                // data object to submit
		                var d = conf.data( request );
		                if ( d ) {
		                    $.extend( request, d );
		                }
		            }
		            else if ( $.isPlainObject( conf.data ) ) {
		                // As an object, the data given extends the default
		                $.extend( request, conf.data );
		            }
		 
		            settings.jqXHR = $.ajax( {
		                "type":     conf.method,
		                "url":      conf.url,
		                "data":     request,
		                "dataType": "json",
		                "cache":    false,
		                "success":  function ( json ) {
		                    cacheLastJson = $.extend(true, {}, json);
		 
		                    if ( cacheLower != drawStart ) {
		                        json.data.splice( 0, drawStart-cacheLower );
		                    }
		                    json.data.splice( requestLength, json.data.length );
		                     
		                    drawCallback( json );
		                }
		            } );
		        }
		        else {
		            json = $.extend( true, {}, cacheLastJson );
		            json.draw = request.draw; // Update the echo for each response
		            json.data.splice( 0, requestStart-cacheLower );
		            json.data.splice( requestLength, json.data.length );
		 
		            drawCallback(json);
		        }
		    }
		};
		 
		// Register an API method that will empty the pipelined data, forcing an Ajax
		// fetch on the next draw (i.e. `table.clearPipeline().draw()`)
		$.fn.dataTable.Api.register( 'clearPipeline()', function () {
		    return this.iterator( 'table', function ( settings ) {
		        settings.clearCache = true;
		    } );
		} );
	</script>

	<script>
		function lpad(n, width, z)
		{
			z = z || '0';
			n = n + '';
			return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
		};

		window.onload = function()
		{
			var topBar =
			{
				cerrarSesion : document.getElementById("cerrar-sesion")
			};
			var modal =
			{
				cargando : document.getElementById("cargando-modal")
			};

			topBar.cerrarSesion.onclick = function()
			{
				$.post( "../php/api.php",
				{
					accion: "cerrar-sesion",
				}, function( data )
				{
				  	if ( data.status === "OK" )
				  	{
				  		window.location.href = "../";
				  	}
				}, "json");
			};

			// Inicializar Datatables
		    $('#dt-recetas').dataTable( {
		    	"language":
		    	{
					"url": "json/datatables.spanish.lang.json"
				},
				"columns": [{"className":"hide-for-small-only"},null,null,null,null,null,null],
				"pageLength": 25,
		        "processing": true,
		        "serverSide": true,
		        "ajax": $.fn.dataTable.pipeline( {
		            url: "../php/scripts/server_processing.php?o=2",
		            pages: 5 // number of pages to cache
		        })
		    });

			// Inicializar los input con Mask
			$("#fechahora-elaboracion").mask("00/00/0000 00:00", {clearIfNotMatch: true} );
			$(".cantidad").mask("000000000",
			{
				onKeyPress: function(cep, event, currentField, options)
				{
					var _this = currentField[0];
					if(parseInt(cep) > parseInt(_this.dataset.cantidadDisponible))
					{
						alert("Lo sentimos, no es posible proceder.\nSobrepaso la cantidad disponible.");
						_this.value = _this.dataset.cantidad || _this.dataset.cantidadDisponible;
					};
				}
			});

			// Evento para abrir el modal de la receta e inicializar el campo de fecha y hora.
			$("#elaborar-receta").click(function()
			{
				// Limpiar el form primero.
				$("#fechahora-elaboracion").val("");
		  		$("#folio").val("");
		  		$("#autocomplete-medico").val("");
		  		$("#autocomplete-medico-id").val("");
		  		$("#autocomplete-trabajador").val("");
		  		$("#autocomplete-trabajador-id").val("");
		  		$("#table-medicamentos-receta tbody").html("");
		  		$("#receta-id").val("");

		  		// Inicializar la fechahora con la actual.
				var date = new Date();
				var dia = lpad(date.getDate(),2);
				var mes = lpad(date.getMonth() + 1,2);
				var temporada = date.getFullYear();
				var hora = lpad(date.getHours(),2);
				var minutos = lpad(date.getMinutes(),2);
				$("#fechahora-elaboracion").val(dia.toString()+mes.toString()+temporada.toString()+hora.toString()+minutos.toString());
				$("#fechahora-elaboracion").unmask().mask("00/00/0000 00:00", {clearIfNotMatch: true} );
				$("#receta-accion").val("elaborar-receta");
				$("#receta-completa").prop("checked", true);
				$("#text-estatus-receta").text("COMPLETA");
				$("#receta-modal").foundation("reveal", "open");
			});

			$(document).on("click", "a.editar-receta", function()
			{
				$("#table-medicamentos-receta tbody").html("");
				$("#receta-accion").val("editar-receta");

				$.post( "../php/api.php",
				{
					accion: "obtener-receta",
					id: this.dataset.id
				}, function( data )
				{
				  	if ( data.status === "OK" )
				  	{
				  		var resultado = data.resultado;
				  		var receta = resultado.receta;
				  		var medicamentos = resultado.medicamentos;

				  		$("#fechahora-elaboracion").val(receta.fechahora);
				  		$("#fechahora-elaboracion").unmask().mask("00/00/0000 00:00", {clearIfNotMatch: true} ).focus();
				  		$("#folio").val(receta.folio);
				  		$("#autocomplete-medico").val(receta.medico);
				  		$("#autocomplete-medico")[0].dataset.id = receta.id_medico;
				  		$("#autocomplete-medico-id").val(receta.id_medico);
				  		$("#autocomplete-trabajador").val(receta.trabajador);
				  		$("#autocomplete-trabajador")[0].dataset.id = receta.id_trabajador;
				  		$("#autocomplete-trabajador-id").val(receta.id_trabajador);
				  		$("#receta-completa")[0].checked = receta.es_completa === "true" ? true : false;
				  		$("#receta-completa").trigger("change");

				  		var id_beneficiario = receta.id_beneficiario;

				  		$.post( "../php/api.php",
						{
							accion: "obtener-beneficiarios",
							id_trabajador: receta.id_trabajador
						}, function( data )
						{
						  	if ( data.status === "OK" )
						  	{
						  		$("#beneficiarios").empty();
						  		var beneficiarios = data.resultado;

						  		for (var i = 0; i < beneficiarios.length; i++)
						  		{
						  			if (beneficiarios[i].activo === "0")
						  			{
						  				alert("El beneficiario con nombre " + beneficiarios[i].nombre + " está INACTIVO.\nFavor de solicitarle que acuda a la Dirección de Planeación DIF para actualizar sus datos.");
						  			}
						  			else
						  			{
						  				$("#beneficiarios").append("<option value='"+beneficiarios[i].id+"'>"+beneficiarios[i].nombre+"</option>");
						  			};
						  		};

						  		$("#beneficiarios").val(id_beneficiario);
						  	};

						  	$("#receta-modal").foundation("reveal", "open");
						}, "json");

				  		for (var i = 0; i < medicamentos.length; i++)
				  		{
				  			var index = i + 1;
				  			$("#table-medicamentos-receta tbody").append("<tr>"+
								"<td style='width:3%;'>"+index+"</td>"+
								"<td style='width:50%;'>"+medicamentos[i].nombre+"</td>"+
								"<td style='width:5%;'>"+medicamentos[i].cantidad_disponible+"</td>"+
								"<td style='width:35%;'><input type='text' name='cantidad-"+index+"' value='"+medicamentos[i].cantidad+"' class='cantidad' onkeyup='' data-cantidad='"+medicamentos[i].cantidad+"' data-cantidad-disponible='"+medicamentos[i].cantidad_disponible+"' required></td>"+
								"<td style='width:5%;' class='text-center'><a href='#' style='font-size:28px;' onclick='this.parentNode.parentNode.remove();'>&#215;</a><input type='hidden' name='id-"+index+"' value='"+medicamentos[i].id_medicamento+"'><input type='hidden' name='id-mr-"+index+"' value='"+medicamentos[i].id+"'></td>"+
							"</tr>");
				  		};

				  		//$("#receta-completa").prop("checked", false);
				  		//$("#text-estatus-receta").text("PENDIENTE");
				  		$("#receta-id").val(receta.id);
				  	};
				}, "json");
			});

			// Evento del Reveal para modificar la fecha y hora de elaboracion
			// $(document).on("opened.fndtn.reveal", "#receta-modal", function ()
			// {
			// 	var date = new Date();
			// 	var dia = lpad(date.getDate(),2);
			// 	var mes = lpad(date.getMonth() + 1,2);
			// 	var temporada = date.getFullYear();
			// 	var hora = lpad(date.getHours(),2);
			// 	var minutos = lpad(date.getMinutes(),2);
			// 	$("#fechahora-elaboracion").val(dia.toString()+mes.toString()+temporada.toString()+hora.toString()+minutos.toString());
			// 	$("#fechahora-elaboracion").unmask().mask("00/00/0000 00:00", {clearIfNotMatch: true} ).focus();
			// });

			$("#autocomplete-medico").autocomplete(
			{
			    source: "../php/autocomplete.php?o=1",
			    minLength: 2,
			    select: function( event, ui )
			    {
			    	this.dataset.id = ui.item.id;
			    	$("#autocomplete-medico-id").val(this.dataset.id);
			    	return;
			    },
			    change: function( event, ui )
			    {
			    	if (ui.item === null)
			    	{
			    		this.dataset.id = 0;
			    		$("#autocomplete-medico").val("");
			    		$("#autocomplete-medico-id").val(this.dataset.id);
			    	}
			    }
			});

			$("#autocomplete-trabajador").autocomplete(
			{
			    source: "../php/autocomplete.php?o=2",
			    minLength: 2,
			    select: function( event, ui )
			    {
			    	this.dataset.id = ui.item.id;
			    	$("#autocomplete-trabajador-id").val(this.dataset.id);

			    	$.post( "../php/api.php",
					{
						accion: "obtener-beneficiarios",
						id_trabajador: ui.item.id
					}, function( data )
					{
					  	if ( data.status === "OK" )
					  	{
					  		$("#beneficiarios").empty();
					  		var beneficiarios = data.resultado;

					  		for (var i = 0; i < beneficiarios.length; i++)
					  		{
					  			if (beneficiarios[i].activo === "0")
					  			{
					  				alert("El beneficiario con nombre " + beneficiarios[i].nombre + " está INACTIVO.\nFavor de solicitarle que acuda a la Dirección de Planeación DIF para actualizar sus datos.");
					  			}
					  			else
					  			{
					  				$("#beneficiarios").append("<option value='"+beneficiarios[i].id+"'>"+beneficiarios[i].nombre+"</option>");
					  			};
					  		};
					  	};
					}, "json");
			    	return;
			    },
			    change: function( event, ui )
			    {
			    	if (ui.item === null)
			    	{
			    		this.dataset.id = 0;
			    		$("#autocomplete-trabajador").val("");
			    		$("#autocomplete-trabajador-id").val(this.dataset.id);
			    		$("#beneficiarios").empty();
			    	}
			    }
			}).autocomplete( "instance" )._renderItem = function( ul, item ) {
		      	return $( "<li style='border-bottom:1px solid grey'>" )
		        .append( "<a>" + item.value + "<br><small><strong>" + item.label + "</strong></small></a>" )
		        .appendTo( ul );
		    };

			$("#autocomplete-medicamento").autocomplete(
			{
			    source: "../php/autocomplete.php?o=3",
			    minLength: 2,
			    select: function( event, ui )
			    {
					var index = $("#table-medicamentos-receta tbody tr").length + 1;

					if ( parseInt(ui.item.cantidad_disponible) === 0)
					{
						alert("Lo sentimos.\nNo puedes elegir este producto ya que se encuentra agotado.");
						ui.item.value = "";
						return;
					};

					$("#table-medicamentos-receta tbody").append("<tr>"+
						// "<td style='width:2%;' class='text-center'><input type='checkbox' name='existe-"+index+"' style='width:24px;height:24px;' checked></td>"+
						"<td style='width:3%;'>"+index+"</td>"+
						"<td style='width:50%;'>"+ui.item.value+"</td>"+
						"<td style='width:5%;'>"+ui.item.cantidad_disponible+"</td>"+
						"<td style='width:35%;'><input type='text' name='cantidad-"+index+"' class='cantidad' value=1 data-cantidad-disponible='"+ui.item.cantidad_disponible+"' required></td>"+
						"<td style='width:5%;' class='text-center'><a href='#' style='font-size:28px;' onclick='this.parentNode.parentNode.remove();'>&#215;</a><input type='hidden' name='id-"+index+"' value='"+ui.item.id+"'></td>"+
					"</tr>");

					ui.item.value = "";
			    	return;
			    }
			}).autocomplete( "instance" )._renderItem = function( ul, item ) {
		      	return $( "<li style='border-bottom:1px solid grey'>" )
		        .append( "<a>" + item.value + "<br><span style='font-size:12px;'>Cantidad: <strong>" + item.cantidad_disponible + "</strong>, Tipo: <strong>" + item.tipo + "</strong></span></a>" )
		        .appendTo( ul );
		    };

			$("#receta-form").submit(function()
			{
				if ($("#autocomplete-medico").val() === ""
					|| $("#autocomplete-medico")[0].dataset.id === undefined
					|| $("#autocomplete-medico-id").val() === "")
				{
					$("#autocomplete-medico").focus();
					return false;
				};

				if ( ($("#autocomplete-trabajador").val() === ""
					|| $("#autocomplete-trabajador")[0].dataset.id === undefined || $("#autocomplete-trabajador")[0].dataset.id === "0"
					|| $("#autocomplete-trabajador-id").val() === "" || $("#autocomplete-trabajador-id").val() === "0") )
				{
					$("#autocomplete-trabajador").focus();
					return false;
				}
				else if ($("#autocomplete-trabajador").val() === "")
				{
					$("#autocomplete-trabajador").focus();
					return false;
				}

				// if ($("#table-medicamentos-receta tbody tr").length === 0)
				// {
				// 	$("#autocomplete-medicamento").focus();
				// 	return false;
				// };

				if ($("#receta-accion").val() === "elaborar-receta")
				{
					var confirmarEnvio = confirm("Estás a punto de elaborar una nueva receta.\n¿Deseas continuar?");
				}
				else
				{
					var confirmarEnvio = confirm("Estás a punto de editar la información de una receta.\n¿Deseas continuar?");
				};

				if (confirmarEnvio)
				{
					return true;
				};

				return false;
			});

			$(document).on("click", "a.ver-mas-receta", function()
			{
				$.post( "../php/api.php",
				{
					accion: "ver-mas-receta",
					id: this.dataset.id
				}, function( data )
				{
				  	if ( data.status === "OK" )
				  	{
				  		$("#table-medicamentos-ver-mas-receta tbody").html("");

				  		var receta = data.resultado.receta;
				  		var medicamentos = data.resultado.medicamentos;

				  		$("#ver-mas-receta-fechahora").html(receta.fechahora);
				  		$("#ver-mas-receta-folio").html(receta.folio);
				  		$("#ver-mas-receta-medico").html(receta.medico);
				  		$("#ver-mas-receta-trabajador").html(receta.trabajador);
				  		$("#ver-mas-receta-empresa").html(receta.empresa);
				  		$("#ver-mas-receta-estatus").html(receta.estatus);

				  		if (medicamentos.length === 0)
				  		{
				  			$("#table-medicamentos-ver-mas-receta tbody").append("<tr><td colspan=3>No hay resultados.</td></tr>");
				  		}
				  		else
				  		{
				  			for (var i = 0; i < medicamentos.length; i++)
					  		{
					  			$("#table-medicamentos-ver-mas-receta tbody").append("<tr>"+
					  				"<td>"+(i+1)+"</td>"+
					  				"<td>"+medicamentos[i].medicamento+"</td>"+
					  				// "<td>"+medicamentos[i].cantidad_disponible+"</td>"+
					  				"<td>"+medicamentos[i].cantidad+"</td>"+
					  			"</tr>");
					  		};
				  		};

				  		$("#ver-mas-receta-modal").foundation("reveal", "open");
				  	};
				}, "json");
			});

			$(document).on("closed.fndtn.reveal", "#receta-modal", function ()
			{
				$("#beneficiarios").empty();
			});

			$(document).on("opened.fndtn.reveal", "#receta-modal", function ()
			{
				if ($("#receta-accion").val() === "elaborar-receta")
				{
					$("#receta-modal-titulo").html("Elaborar Nueva Receta");
				}
				else
				{
					$("#receta-modal-titulo").html("Editar Receta");
				};

				$("#folio").focus();
			});
		};
	</script>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-47062938-3', 'auto');
		ga('send', 'pageview');
	</script>
</body>
</html>