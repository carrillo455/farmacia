<?php
	session_name("farmacia_dif");
	session_start();

	if ( !isset($_SESSION["usuario"]) )
	{
		header("Location: ../");
		exit;
	}

	$usuario_nombre = $_SESSION["usuario"]["nombre"];
	$permisos = $_SESSION["usuario"]["permisos"];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>DIF Farmacia | Sistema de Control de Inventarios</title>
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/foundation.min.css" />
    <link rel="stylesheet" href="../css/dataTables.foundation.css">
    <script src="../js/vendor/modernizr.js"></script>
    <style>
    	tbody tr { -webkit-transition: background-color 500ms ease-out 200ms;
    	-moz-transition: background-color 500ms ease-out 200ms;
    	-o-transition: background-color 500ms ease-out 200ms;
    	transition: background-color 500ms ease-out 200ms; }
    	.highlight { background-color: #FAA524 !important; }
    </style>
</head>
<body>
	<nav id="top-bar-principal" class="top-bar" data-topbar>
		<ul class="title-area">
			<li class="name">
				<h1><a href="#">DIF Farmacia</a></h1>
			</li>
			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<ul class="right">
				<li><a href="index.php?<?php echo $usuario_nombre; ?>">Medicamentos</a></li>
				<li><a href="recetas.php?<?php echo $usuario_nombre; ?>">Recetas</a></li>
				<!-- <li><a href="reportes.php">Reportes</a></li> -->
				<!-- <li class="has-dropdown">
					<a href="eventos.php">Eventos</a>
					<ul class="dropdown">
						<li><a class="evento" href="#">Crear Evento</a></li>
						<li><a class="evento" href="#">Editar Evento</a></li>
					</ul>
				</li> -->
				<li><a id="cerrar-sesion" href="#">Cerrar Sesión</a></li>
			</ul>

			<ul class="left hide-for-small-only">
				<li><a href="#"><?php echo "Bienvenido <b>$usuario_nombre</b>"; ?></a></li>
			</ul>
		</section>
	</nav>

	<header>
		<div class="row">
			<div class="large-12 medium-12 small-12 columns">
				<h2 style="margin-bottom:0;">Medicamentos</h2>
			</div>

			<div id="alertas" class="large-12 columns hide">
				
			</div>

			<div class="large-8 medium-8 small-12 columns">
				<p class="subheader">
					Da clic en <strong>Agregar Nuevo Medicamento</strong> para ingresar manualmente la información del nuevo medicamento.
				</p>
			</div>

			<div class="large-4 medium-4 small-12 columns">
				<a id="agregar-medicamento" href="#" class="small button expand">Agregar Nuevo Medicamento</a>
			</div>
		</div>
	</header>

	<div class="row">
		<div class="large-12 columns">
			<hr>
		</div>
	</div>

	<div class="row">
		<div class="large-12 columns">
			<table id="dt-medicamentos" class="tdisplay compact" style="width: 100%;">
				<thead>
					<th style="width:5%;">#</th>
					<th style="width:40%;">Medicamento</th>
					<th style="width:5%">Min.</th>
					<th style="width:5%">Max.</th>
					<th style="width:15%;">Tipo</th>
					<th style="width:10%;">Cantidad</th>
					<th style="width:5%;"></th>
					<th style="width:5%;"></th>
					<th style="width:10%;"></th>
				</thead>
			</table>
		</div>
	</div>

	<div id="medicamento-modal" class="reveal-modal" data-reveal aria-labelledby="medicamento-modal-titulo" aria-hidden="true" role="dialog">
	  	<h2 id="medicamento-modal-titulo"></h2>
	  	<div class="row">
	  		<div class="large-12 columns">
	  			<form id="medicamento-form">
	  				<div class="row">
	  					<div class="large-12 columns">
	  						<label for="medicamento-fecha">Fecha de Recepción</label>
	  						<input id="medicamento-fecha" name="medicamento-fecha" class="fecha" type="text">
	  					</div>

	  					<div class="large-12 columns">
	  						<label for="medicamento-nombre">Nombre del Medicamento</label>
	  						<input id="medicamento-nombre" name="medicamento-nombre" type="text" required>
	  					</div>

	  					<div class="large-6 medium-6 small-12 columns">
	  						<label for="medicamento-tipo">Tipo de Medicamentos</label>
	  						<select id="medicamento-tipo" name="medicamento-tipo" data-tipos></select>
	  					</div>

	  					<div class="large-3 medium-3 small-6 columns">
	  						<label for="medicamento-minimo">Mínimo</label>
	  						<input id="medicamento-minimo" name="medicamento-minimo" type="text">
	  					</div>

	  					<div class="large-3 medium-3 small-6 columns">
	  						<label for="medicamento-maximo">Máximo</label>
	  						<input id="medicamento-maximo" name="medicamento-maximo" type="text">
	  					</div>

	  					<div class="large-12 columns">
	  						<label for="medicamento-cantidad">Cantidad</label>
	  						<input id="medicamento-cantidad" name="medicamento-cantidad" class="cantidad" type="text" required>
	  					</div>

	  					<div class="large-12 columns">
	  						<label for="medicamento-tipo-movimiento">Tipo de Movimiento</label>
	  						<select id="medicamento-tipo-movimiento" name="medicamento-tipo-movimiento" data-tipos-movimiento></select>
	  					</div>

	  					<div id="contenedor-observaciones" class="large-12 columns">
	  						<label for="observaciones">Observaciones</label>
	  						<textarea id="medicamento-observaciones" name="medicamento-observaciones" cols="30" rows="10"></textarea>
	  					</div>

	  					<div class="large-offset-8 large-4 medium-offset-8 medium-4 small-12 columns end">
	  						<input id="medicamento-enviar" type="submit" class="small button expand" value="Enviar">
	  					</div>
	  				</div>
	  			</form>
	  		</div>
	  	</div>
	  	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	</div>

	<div id="alta-baja-medicamento-modal" class="reveal-modal" data-reveal aria-labelledby="alta-baja-medicamento-modal-titulo" aria-hidden="true" role="dialog">
	  	<h2 id="alta-baja-medicamento-modal-titulo"></h2>
	  	<div class="row">
	  		<div class="large-12 columns">
	  			<form id="alta-baja-medicamento-form">
	  				<div class="row">
	  					<div class="large-12 columns">
	  						<label for="alta-baja-medicamento-fecha">Fecha de Recepción</label>
	  						<input id="alta-baja-medicamento-fecha" name="alta-baja-medicamento-fecha" class="fecha" type="text">
	  					</div>

	  					<div class="large-12 columns">
	  						<label for="alta-baja-medicamento-nombre">Nombre del Medicamento</label>
	  						<p><strong id="alta-baja-medicamento-nombre"></strong></p>
	  					</div>

	  					<div class="large-12 columns">
	  						<label for="alta-baja-medicamento-cantidad">Cantidad</label>
	  						<input id="alta-baja-medicamento-cantidad" name="medicamento-cantidad" class="cantidad" type="text" required>
	  					</div>

	  					<div class="large-12 columns">
	  						<label for="medicamento-tipo-movimiento">Tipo de Movimiento</label>
	  						<select id="alta-baja-medicamento-tipo-movimiento" name="alta-baja-medicamento-tipo-movimiento" data-tipos-movimiento></select>
	  					</div>

	  					<div class="large-12 columns">
	  						<label for="alta-baja-medicamento-observaciones">Observaciones</label>
	  						<textarea id="alta-baja-medicamento-observaciones" name="medicamento-observaciones" cols="30" rows="10"></textarea>
	  					</div>

	  					<div class="large-offset-8 large-4 medium-offset-8 medium-4 small-12 columns end">
	  						<input id="alta-baja-medicamento-enviar" type="submit" class="small button expand" value="Enviar">
	  					</div>
	  				</div>
	  			</form>
	  		</div>
	  	</div>
	  	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	</div>

	<div id="min-max-modal" class="small reveal-modal" data-reveal aria-labelledby="min-max-modal-titulo" aria-hidden="true" role="dialog">
	  	<h2 id="min-max-modal-titulo">¡ALERTA!</h2>
	  	<div class="row">
	  		<div class="large-12 columns">
	  			<h4>Se ha(n) detectado <b id="min-cant"></b> medicamentos con existencias MINIMAS y <b id="max-cant"></b> con MAXIMAS.</h4>
	  		</div>

	  		<div class="large-12 columns end">
	  			<a href="reportes.php?minMax" class="tiny success button right">Ver Reporte de Mínimos y Máximos</a>
	  		</div>
	  	</div>
	  	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	</div>

	<div id="cargando-modal" class="tiny reveal-modal" data-reveal aria-hidden="true" role="dialog">
		<p class="text-center">Cargando... <img src="../css/images/cargando.gif"></p>
	</div>

	<script src="../js/vendor/jquery.js"></script>
	<script src="../js/vendor/jquery.mask.min.js"></script>
	<script src="../js/vendor/jquery.dataTables.min.js"></script>
	<script src="../js/vendor/dataTables.foundation.js"></script>
	<script src="../js/foundation.min.js"></script>
  	<script src="../js/foundation/foundation.topbar.js"></script>
  	<script src="../js/foundation/foundation.reveal.js"></script>
  	<script>$(document).foundation({
  		topbar :
  		{
			custom_back_text: false,
			is_hover: false,
			mobile_show_parent_link: false
		},
		reveal :
		{
			animation_speed: 0,
			close_on_background_click: false
		}
  	});</script>
	<script>
		function lpad(n, width, z)
		{
			z = z || '0';
			n = n + '';
			return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
		};

		window.onload = function()
		{
			// Inicializar Datatables
		    $('#dt-medicamentos').dataTable( {
		    	"language":
		    	{
					"url": "json/datatables.spanish.lang.json"
				},
				"pageLength": 25,
		        "processing": true,
		        "serverSide": true,
		        "ajax": '../php/scripts/server_processing.php?o=1',
		        "columns":
		        [
		        	null,
		        	{"className" : "medicamento-nombre"},
		        	null,
		        	null,
		        	{"className" : "medicamento-tipo"},
		        	null,
		        	null,
		        	null,
		        	null
		        ]
		    });

		    // Inicializar los tipos de medicamento
		    $.post( "../php/api.php",
			{
				accion: "obtener-tipos-medicamentos"
			}, function( data )
			{
			  	if ( data.status === "OK" )
			  	{
			  		var tipos = data.resultado;
			  		
			  		for (var i = 0; i < tipos.length; i++)
			  		{
			  			$("[data-tipos]").append("<option value='"+tipos[i].id+"'>"+tipos[i].nombre+"</option>");
			  		};
			  	};
			}, "json");

		    // Inicializar los tipos de movimientos del medicamento
		    $.post( "../php/api.php",
			{
				accion: "obtener-tipos-movimiento-medicamento"
			}, function( data )
			{
			  	if ( data.status === "OK" )
			  	{
			  		var movimientos = data.resultado;
			  		
			  		for (var i = 0; i < movimientos.length; i++)
			  		{
			  			$("[data-tipos-movimiento]").append("<option value='"+movimientos[i].id+"'>"+movimientos[i].nombre+"</option>");
			  		};
			  	};
			}, "json");

			// Inicializar el Mask en los inputs de cantidad
			var date = new Date();
			var dia = date.getDate();
			var mes = date.getMonth() + 1;
			var temporada = date.getFullYear();

			$(".fecha").mask("00/00/0000", {clearIfNotMatch: true} ).val(lpad(dia,2,"0")+"/"+lpad(mes,2,"0")+"/"+temporada);
			$(".cantidad").mask("000000000");
			
			var topBar =
			{
				cerrarSesion : document.getElementById("cerrar-sesion")
			};
			
			var modal =
			{
				evento : document.getElementById("eventos-dia-modal"),
				cargando : document.getElementById("cargando-modal")
			};

			// Inicio de asignacion de eventos
			topBar.cerrarSesion.onclick = function()
			{
				$.post( "../php/api.php",
				{
					accion: "cerrar-sesion",
				}, function( data )
				{
				  	if ( data.status === "OK" )
				  	{
				  		window.location.href = "../";
				  	}
				}, "json");
			};

			$("#agregar-medicamento").click(function()
			{
				$("#medicamento-modal-titulo").html("Agregar Nuevo Medicamento");
				$("#medicamento-nombre").val("");
				$("[data-tipos] option:first").prop("selected", true);
				$("#medicamento-minimo").val("");
				$("#medicamento-maximo").val("");
				$("#medicamento-cantidad").val("");
				$("#medicamento-form")[0].dataset.accion = "agregar-medicamento";
				$("#medicamento-modal").foundation("reveal", "open");
			});

			$(document).on("click", "a.editar-tipo-medicamento", function()
			{
				var tipo = this.dataset.tipo === "2" ? "GENERICO" : "PATENTE";
				var confirmarEnvio = confirm("Estás a punto de cambiar el tipo de medicamento a "+tipo+".\n¿Deseas continuar?");

				if (confirmarEnvio)
				{
					var _this = this;
					$("#cargando-modal").foundation("reveal", "open");

					$.post( "../php/api.php",
					{
						accion: "editar-tipo-medicamento",
						id: this.dataset.id,
						tipo: this.dataset.tipo
					}, function( data )
					{
					  	if ( data.status === "OK" )
					  	{
					  		_this.parentNode.parentNode.querySelector("td.medicamento-tipo").innerHTML = data.resultado;
					  		$("#cargando-modal").foundation("reveal", "close");
					  	}
					}, "json");
				};
			});

			$(document).on("click", "a.alta-medicamento", function()
			{
				$("#alta-baja-medicamento-modal-titulo").html("Alta de Medicamento");
				$("#alta-baja-medicamento-nombre").html(this.parentNode.parentNode.querySelector("td.medicamento-nombre").textContent);
				$("#alta-baja-medicamento-form")[0].dataset.id = this.dataset.id;
				$("#alta-baja-medicamento-form")[0].dataset.accion = "alta-medicamento";
				$("#alta-baja-medicamento-modal").foundation("reveal", "open");

				this.parentNode.parentNode.classList.add("highlight");
			});

			$(document).on("click", "a.baja-medicamento", function()
			{
				$("#alta-baja-medicamento-modal-titulo").html("Baja de Medicamento");
				$("#alta-baja-medicamento-nombre").html(this.parentNode.parentNode.querySelector("td.medicamento-nombre").textContent);
				$("#alta-baja-medicamento-form")[0].dataset.id = this.dataset.id;
				$("#alta-baja-medicamento-form")[0].dataset.accion = "baja-medicamento";
				$("#alta-baja-medicamento-modal").foundation("reveal", "open");

				this.parentNode.parentNode.classList.add("highlight");
			});

			$(document).on("click", "a.editar-medicamento", function()
			{
				$("#medicamento-modal-titulo").html("Editar Medicamento");
				$("#medicamento-form")[0].dataset.id = this.dataset.id;
				$("#medicamento-form")[0].dataset.accion = "editar-medicamento";

				$("#cargando-modal").foundation("reveal", "open");

				$.post( "../php/api.php",
				{
					accion: "obtener-medicamento",
					id: this.dataset.id
				}, function( data )
				{
				  	if ( data.status === "OK" )
				  	{
				  		var medicamento = data.resultado;
				  		$("#medicamento-nombre").val(medicamento.nombre);
				  		$("[data-tipos]").val(medicamento.tipo);
				  		$("#medicamento-minimo").val(medicamento.minimo);
				  		$("#medicamento-maximo").val(medicamento.maximo);
						$("#medicamento-cantidad").val(medicamento.cantidad);

						$("#cargando-modal").foundation("reveal", "close");

						setTimeout(function() { $("#medicamento-modal").foundation("reveal", "open"); }, 1);
				  	}
				}, "json");

				this.parentNode.parentNode.classList.add("highlight");
			});

			$(document).on("opened.fndtn.reveal", "#medicamento-modal", function ()
			{
				$("#medicamento-fecha").focus();
			});

			$(document).on("closed.fndtn.reveal", "#medicamento-modal", function ()
			{
				delete $("#medicamento-form")[0].dataset.id;
			  	delete $("#medicamento-form")[0].dataset.accion;
			  	$("tr.highlight").removeClass("highlight");
			});

			$(document).on("opened.fndtn.reveal", "#alta-baja-medicamento-modal", function ()
			{
				$("#alta-baja-medicamento-observaciones").val("");
				$("#alta-baja-medicamento-cantidad").val("").focus();
			});

			$(document).on("closed.fndtn.reveal", "#alta-baja-medicamento-modal", function ()
			{
				$("tr.highlight").removeClass("highlight");
			});

			$("#medicamento-form").submit(function()
			{
				if ($("#medicamento-fecha").val() === "")
				{
					$("#medicamento-fecha").focus();
					return false;
				};

				if ($("#medicamento-nombre").val() === "")
				{
					$("#medicamento-nombre").focus();
					return false;
				};

				if ($("#medicamento-cantidad").val() === "")
				{
					$("#medicamento-cantidad").focus();
					return false;
				};

				if (this.dataset.accion === "agregar-medicamento")
				{
					var confirmarEnvio = confirm("Estás a punto de agregar un nuevo medicamento.\n¿Deseas continuar?");
					$("div#alertas").html("<div data-alert class='alert-box success'>¡El medicamento fue <strong>agregado</strong> con éxito!<a href='#' class='close'>&times;</a></div>");
				}
				else
				{
					var confirmarEnvio = confirm("Estás a punto de editar la información de un medicamento.\n¿Deseas continuar?");
					$("div#alertas").html("<div data-alert class='alert-box success'>¡El medicamento fue <strong>editado</strong> con éxito!<a href='#' class='close'>&times;</a></div>");
				};

				if (confirmarEnvio)
				{
					$("#cargando-modal").foundation("reveal", "open");

					$.post( "../php/api.php",
					{
						accion: this.dataset.accion,
						id: this.dataset.id,
						"medicamento-fecha": $("#medicamento-fecha").val(),
						"medicamento-nombre": $("#medicamento-nombre").val(),
						"medicamento-minimo": $("#medicamento-minimo").val(),
						"medicamento-maximo": $("#medicamento-maximo").val(),
						"medicamento-tipo": $("#medicamento-tipo").val(),
						"medicamento-cantidad": $("#medicamento-cantidad").val(),
						"medicamento-tipo-movimiento": $("#medicamento-tipo-movimiento").val(),
						"medicamento-observaciones": $("#medicamento-observaciones").val()
					}, function( data )
					{
					  	if ( data.status === "OK" )
					  	{
							$("#cargando-modal").foundation("reveal", "close");
							$("#medicamento-modal").foundation("reveal", "close");

							$("div#alertas").removeClass("hide");

							$("#dt-medicamentos").DataTable().search( data.resultado ).draw();
					  	}
					  	else if ( data.status === "ERROR" )
					  	{
					  		$("div#alertas").html("<small class='error'>¡Oops! Algo salió mal. Favor de intentar hacer la operación de nuevo.</small>").removeClass("hide");
					  	}
					  	else
					  	{
					  		window.location.reload();
					  	};
					}, "json");
				};

				return false;
			});

			$("#alta-baja-medicamento-form").submit(function()
			{
				if ($("#alta-baja-medicamento-fecha").val() === "")
				{
					$("#alta-baja-medicamento-fecha").focus();
					return false;
				};

				if ($("#alta-baja-medicamento-cantidad").val() === "")
				{
					$("#alta-baja-medicamento-cantidad").focus();
					return false;
				};

				if (this.dataset.accion === "alta-medicamento")
				{
					var confirmarEnvio = confirm("Estás a punto de SUMAR la cantidad al medicamento.\n¿Deseas continuar?");
					$("div#alertas").html("<div data-alert class='alert-box success'>¡La <strong>cantidad del medicamento</strong> fue <strong>aumentada</strong> con éxito!<a href='#' class='close'>&times;</a></div>");					
				}
				else
				{
					var confirmarEnvio = confirm("Estás a punto de RESTAR la cantidad del medicamento.\n¿Deseas continuar?");
					$("div#alertas").html("<div data-alert class='alert-box success'>¡La <strong>cantidad del medicamento</strong> fue <strong>disminuida</strong> con éxito!<a href='#' class='close'>&times;</a></div>");
				}

				if (confirmarEnvio)
				{
					$("#cargando-modal").foundation("reveal", "open");

					$.post( "../php/api.php",
					{
						accion: this.dataset.accion,
						id: this.dataset.id,
						"medicamento-fecha": $("#alta-baja-medicamento-fecha").val(),
						"medicamento-cantidad": $("#alta-baja-medicamento-cantidad").val(),
						"medicamento-tipo-movimiento": $("#alta-baja-medicamento-tipo-movimiento").val(),
						"medicamento-observaciones": $("#alta-baja-medicamento-observaciones").val()
					}, function( data )
					{
					  	if ( data.status === "OK" )
					  	{
							$("#cargando-modal").foundation("reveal", "close");
							$("#alta-baja-medicamento-modal").foundation("reveal", "close");

							$("#dt-medicamentos").DataTable().draw( false );
							$("div#alertas").removeClass("hide");
					  	}
					  	else if ( data.status === "ERROR" )
					  	{
					  		$("#cargando-modal").foundation("reveal", "close");
					  		$("div#alertas").html("<small class='error'>¡Oops! Algo salió mal. Favor de intentar hacer la operación de nuevo.</small>").removeClass("hide");
					  	}
					  	else
					  	{
					  		window.location.reload();
					  	};
					}, "json");
				};

				return false;
			});

			// Revisar si hay medicamentos en rangos min max.
		    $.post( "../php/api.php",
			{
				accion: "obtener-min-max"
			}, function( data )
			{
			  	if ( data.status === "OK" )
			  	{
			  		var resultado = data.resultado,
			  			min = parseInt(resultado.min),
			  			max = parseInt(resultado.max);

			  		if (min > 0 || max > 0)
			  		{
			  			$("#min-cant").html(min);
			  			$("#max-cant").html(max);
			  			$("#min-max-modal").foundation("reveal", "open");
			  		}
			  	};
			}, "json");
		};
	</script>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-47062938-3', 'auto');
		ga('send', 'pageview');
	</script>
</body>
</html>