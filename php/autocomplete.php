<?php
	if (!isset($_GET["o"]))
	{
		echo "Lo siento :-(";
		exit;
	}
	else
	{
		session_name("farmacia_dif");
		session_start();

		if (!isset($_SESSION["usuario"]))
		{
			header("Location: index.php");
			exit;
		}
	}

	$pdo = new PDO("mysql:host=localhost;dbname=farmacia_dif;charset=utf8", "root", "");
	$opcion = (int) $_GET["o"];

	switch ($opcion)
	{
		case 1: // AUTOCOMPLETE MEDICO
			$term = mb_strtoupper($_GET["term"], "UTF-8"); // DEFINIDO POR JQUERY UI, CONTIENE EL VALOR DE LO QUE ESCRIBIO EN EL AUTOCOMPLETE.
			$term_array = preg_split('#\s+#', $term, null, PREG_SPLIT_NO_EMPTY);
			$term_conditions = "";

			for ($i=0;$i<count($term_array);$i++)
			{
				$term_conditions .= "UPPER(nombre) LIKE '%".$term_array[$i]."%'";
				if ( $i < (count($term_array)-1) )
				{
					$term_conditions .= " AND ";
				}
			}

			$query = $pdo->prepare("SELECT id_medico AS id, UPPER(nombre) AS value
				FROM medicos WHERE $term_conditions AND activo = 1 LIMIT 20");
			$query->execute();
			$resultado = $query->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode($resultado);
		break;

		case 2: // AUTOCOMPLETE TRABAJADOR
			$term = mb_strtoupper($_GET["term"], "UTF-8"); // DEFINIDO POR JQUERY UI, CONTIENE EL VALOR DE LO QUE ESCRIBIO EN EL AUTOCOMPLETE.
			$term_array = preg_split('#\s+#', $term, null, PREG_SPLIT_NO_EMPTY);
			$term_conditions = "";

			for ($i=0;$i<count($term_array);$i++)
			{
				$term_conditions .= "(UPPER(nombre) LIKE '%".$term_array[$i]."%' OR LPAD(clave_emp, 5, '0') LIKE '%".$term_array[$i]."%' OR tipo LIKE '%".$term_array[$i]."%')";
				if ( $i < (count($term_array)-1) )
				{
					$term_conditions .= " AND ";
				}
			}

			$query = $pdo->prepare("SELECT id, TRIM(nombre) AS value, tipo AS label
				FROM empleados_mas_beneficiarios WHERE $term_conditions AND ( es_nomina = 1 AND ( activo = 1 OR TIMESTAMPDIFF(DAY,fecha_nomina,CURDATE()) <= 30 ) OR  ( es_apoyo = 1 AND clave_emp != '' ) ) LIMIT 20");
			$query->execute();
			$resultado = $query->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode($resultado);
		break;

		case 3: // AUTOCOMPLETE MEDICAMENTOS
			$term = mb_strtoupper($_GET["term"], "UTF-8"); // DEFINIDO POR JQUERY UI, CONTIENE EL VALOR DE LO QUE ESCRIBIO EN EL AUTOCOMPLETE.
			$term_array = preg_split('#\s+#', $term, null, PREG_SPLIT_NO_EMPTY);
			$term_conditions = "";

			for ($i=0;$i<count($term_array);$i++)
			{
				$term_conditions .= "(UPPER(m.nombre) LIKE '%".$term_array[$i]."%' OR tm.nombre LIKE '%".$term_array[$i]."%')";
				if ( $i < (count($term_array)-1) )
				{
					$term_conditions .= " AND ";
				}
			}

			$query = $pdo->prepare("SELECT id_medicamento AS id, UPPER(m.nombre) AS value, cantidad AS cantidad_disponible, tm.nombre AS tipo
				FROM medicamentos m INNER JOIN tipos_medicamentos tm ON tm.id_tipo_medicamento = m.id_tipo_medicamento WHERE $term_conditions LIMIT 20");
			$query->execute();
			$resultado = $query->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode($resultado);
		break;

		case 4: // AUTOCOMPLETE SECRETARIAS
			$term = mb_strtoupper($_GET["term"], "UTF-8"); // DEFINIDO POR JQUERY UI, CONTIENE EL VALOR DE LO QUE ESCRIBIO EN EL AUTOCOMPLETE.

			$query = $pdo->prepare("SELECT DISTINCT iddependencia AS id, dependencia AS value FROM deptos_mas_depends WHERE UPPER(dependencia) LIKE '%".$term."%' LIMIT 20");
			$query->execute();
			$resultado = $query->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode($resultado);
		break;

		case 5: // AUTOCOMPLETE DEPARTAMENTOS
			$term = mb_strtoupper($_GET["term"], "UTF-8"); // DEFINIDO POR JQUERY UI, CONTIENE EL VALOR DE LO QUE ESCRIBIO EN EL AUTOCOMPLETE.

			$query = $pdo->prepare("SELECT DISTINCT iddepartamento AS id, departamento AS value FROM deptos_mas_depends WHERE UPPER(departamento) LIKE '%".$term."%' LIMIT 20");
			$query->execute();
			$resultado = $query->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode($resultado);
		break;

		case 6: // AUTOCOMPLETE EMPRESAS
			$term = mb_strtoupper($_GET["term"], "UTF-8"); // DEFINIDO POR JQUERY UI, CONTIENE EL VALOR DE LO QUE ESCRIBIO EN EL AUTOCOMPLETE.

			$query = $pdo->prepare("SELECT DISTINCT tipo AS value FROM empleados_mas_beneficiarios WHERE UPPER(tipo) LIKE '%".$term."%' ORDER BY tipo LIMIT 20");
			$query->execute();
			$resultado = $query->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode($resultado);
		break;

		case 7: // AUTOCOMPLETE NOMBRE DEL TRABAJADOR
			$term = mb_strtoupper($_GET["term"], "UTF-8"); // DEFINIDO POR JQUERY UI, CONTIENE EL VALOR DE LO QUE ESCRIBIO EN EL AUTOCOMPLETE.

			$query = $pdo->prepare("SELECT DISTINCT nombre AS value FROM empleados_mas_beneficiarios WHERE UPPER(nombre) LIKE '%".$term."%' AND es_nomina = 1 ORDER BY nombre LIMIT 20");
			$query->execute();
			$resultado = $query->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode($resultado);
		break;

		case 8: // AUTOCOMPLETE NOMBRE DEL BENEFICIARIO
			$term = mb_strtoupper($_GET["term"], "UTF-8"); // DEFINIDO POR JQUERY UI, CONTIENE EL VALOR DE LO QUE ESCRIBIO EN EL AUTOCOMPLETE.

			$query = $pdo->prepare("SELECT DISTINCT nombre AS value FROM empleados_mas_beneficiarios WHERE UPPER(nombre) LIKE '%".$term."%' AND es_nomina = 0 AND es_apoyo = 0 ORDER BY nombre LIMIT 20");
			$query->execute();
			$resultado = $query->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode($resultado);
		break;

		case 9: // AUTOCOMPLETE NOMBRE DEL APOYO
			$term = mb_strtoupper($_GET["term"], "UTF-8"); // DEFINIDO POR JQUERY UI, CONTIENE EL VALOR DE LO QUE ESCRIBIO EN EL AUTOCOMPLETE.

			$query = $pdo->prepare("SELECT DISTINCT nombre AS value FROM empleados_mas_beneficiarios WHERE UPPER(nombre) LIKE '%".$term."%' AND es_apoyo = 1 ORDER BY nombre LIMIT 20");
			$query->execute();
			$resultado = $query->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode($resultado);
		break;

		case 10: // AUTOCOMPLETE NOMBRE DEL MEDICO
			$term = mb_strtoupper($_GET["term"], "UTF-8"); // DEFINIDO POR JQUERY UI, CONTIENE EL VALOR DE LO QUE ESCRIBIO EN EL AUTOCOMPLETE.

			$query = $pdo->prepare("SELECT DISTINCT nombre AS value FROM medicos WHERE UPPER(nombre) LIKE '%".$term."%' AND activo = 1 ORDER BY nombre LIMIT 20");
			$query->execute();
			$resultado = $query->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode($resultado);
		break;
	}
?>