<?php

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
$option = (int) $_GET["o"];

switch ($option)
{
	case 1:
		// DB table to use
		$table = 'dt_medicamentos';

		// Table's primary key
		$primaryKey = 'id';

		// Array of database columns which should be read and sent back to DataTables.
		// The `db` parameter represents the column name in the database, while the `dt`
		// parameter represents the DataTables column identifier. In this case simple
		// indexes
		$columns = array(
			array( 'db' => 'id', 'dt' => 0 ),
			array( 'db' => 'medicamento', 'dt' => 1 ),
			array( 'db' => 'minimo', 'dt' => 2 ),
			array( 'db' => 'maximo', 'dt' => 3 ),
			array( 'db' => 'tipo', 'dt' => 4 ),
			array( 'db' => 'cantidad',  'dt' => 5 ),
			array( 'db' => 'alta',  'dt' => 6 ),
			array( 'db' => 'baja',  'dt' => 7 ),
			array( 'db' => 'editar',  'dt' => 8 )
			// array(
			// 	'db'        => 'start_date',
			// 	'dt'        => 4,
			// 	'formatter' => function( $d, $row ) {
			// 		return date( 'jS M y', strtotime($d));
			// 	}
			// ),
			// array(
			// 	'db'        => 'salary',
			// 	'dt'        => 5,
			// 	'formatter' => function( $d, $row ) {
			// 		return '$'.number_format($d);
			// 	}
			// )
		);
	break;

	case 2:
		// DB table to use
		$table = 'dt_recetas';

		// Table's primary key
		$primaryKey = 'id';

		// Array of database columns which should be read and sent back to DataTables.
		// The `db` parameter represents the column name in the database, while the `dt`
		// parameter represents the DataTables column identifier. In this case simple
		// indexes
		$columns = array(
			array( 'db' => 'id', 'dt' => 0 ),
			array( 'db' => 'folio', 'dt' => 1 ),
			array( 'db' => 'medico',  'dt' => 2 ),
			array( 'db' => 'trabajador',   'dt' => 3 ),
			array( 'db' => 'empresa',   'dt' => 4 ),
			array( 'db' => 'estatus',   'dt' => 5 ),
			array( 'db' => 'acciones',   'dt' => 6 )
			// array(
			// 	'db'        => 'start_date',
			// 	'dt'        => 4,
			// 	'formatter' => function( $d, $row ) {
			// 		return date( 'jS M y', strtotime($d));
			// 	}
			// ),
			// array(
			// 	'db'        => 'salary',
			// 	'dt'        => 5,
			// 	'formatter' => function( $d, $row ) {
			// 		return '$'.number_format($d);
			// 	}
			// )
		);
	break;
}

// SQL server connection information
$sql_details = array(
	'user' => 'root',
	'pass' => '',
	'db'   => 'farmacia_dif',
	'host' => 'localhost'
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( 'ssp.class.php' );

echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);

