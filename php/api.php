<?php
	session_name("farmacia_dif");
	session_start();

	// CONEXION A LA BASE DE DATOS EN POSTGRES
	//$pdo = new PDO("mysql:host=localhost;dbname=dif_farmacia_altamira;charset=utf8", "diffaltamira", "/s#sWun!");
	$pdo = new PDO("mysql:host=localhost;dbname=farmacia_dif;charset=utf8", "root", "");

	// if ( !isset($_SESSION["usuario"]) || $_SESSION["usuario"]["id"] === "0")
	// {
	// 	echo json_encode(array("accion" => "relogear"));
	// 	exit;
	// }

	$accion = $_POST["accion"];

	switch ($accion)
	{
		case "iniciar-sesion":
			$usuario = $_POST["usuario"];
			$password = $_POST["password"];

			$query = $pdo->prepare("SELECT id_usuario AS id, u.id_nivel_usuario AS id_nivel, nu.nombre AS nivel,
				u.bloqueado AS u_bloqueado, nu.bloqueado AS nu_bloqueado
				FROM usuarios u INNER JOIN niveles_usuarios nu ON u.id_nivel_usuario = nu.id_nivel_usuario
				WHERE usuario = :usuario AND password = :password AND u.borrado = 0 AND nu.borrado = 0");
			$query->execute(array("usuario" => $usuario, "password" => $password));
			$info_usuario = $query->fetch(PDO::FETCH_ASSOC);

			if ( !$info_usuario )
			{
				header("Location: ../index.php?e=1");
				exit;
			}
			else if ( $info_usuario["u_bloqueado"] === "1" || $info_usuario["nu_bloqueado"] === "1" )
			{
				header("Location: ../index.php?e=2");
				exit;
			}

			$id_usuario = $info_usuario["id"];
			$id_nivel = $info_usuario["id_nivel"];
			$nivel = $info_usuario["nivel"];

			$query = $pdo->query("SELECT clave FROM permisos_usuarios pu
				INNER JOIN accesos_usuarios au ON au.id_acceso_usuario = pu.id_acceso_usuario
				WHERE id_nivel_usuario = $id_nivel AND pu.borrado = 0 AND au.borrado = 0");
			$permisos_usuarios = $query->fetchAll(PDO::FETCH_ASSOC);

			// session_name("agenda_sia_2015");
			// session_start();
			$_SESSION["usuario"]["id"] = $id_usuario;
			$_SESSION["usuario"]["nombre"] = $usuario;
			$_SESSION["usuario"]["nivel"] = $nivel;
			$_SESSION["usuario"]["permisos"] = $permisos_usuarios;

			header("Location: ../web/?".$usuario);
			exit;
		break;

		case "cerrar-sesion":
			unset($_SESSION['usuario']);
			session_unset();
			session_destroy();
			session_write_close();
			
			echo json_encode(array("status" => "OK"));
		break;

		case "elaborar-receta":
			if (!isset($_SESSION["usuario"]) || $_SESSION["usuario"]["id"] === "0" || $_SESSION["usuario"]["id"] === 0)
			{
				header("Location: ../index.php?e=3");
				exit;
			}

			$id_usuario = $_SESSION["usuario"]["id"];
			$fechahora = $_POST["fechahora-elaboracion"];
			$folio = $_POST["folio"];
			$id_medico = $_POST["id-medico"];
			$id_trabajador = $_POST["id-trabajador"];
			$id_beneficiario = $_POST["beneficiario"];

			// DAR FORMATO A LA FECHAHORA PARA MYSQL
			$fh = preg_split("/\//", $fechahora, -1); // dd/mm/yyyy hh:mm:ss
			$day = $fh[0];
			$month = $fh[1];
			$year_time = $fh[2];
			$year_time_split = preg_split("/\s/", $year_time, -1);
			$year = $year_time_split[0];
			$time = $year_time_split[1];

			$fecha_elaboracion = $year . "-" . $month . "-" . $day;
			$fechahora_elaboracion = $year . "-" . $month . "-" . $day . " " . $time;

			$medicamentos = array();

			foreach ($_POST as $key => $value)
			{
				if (stripos($key, "cantidad-") === 0) // Es el primer campo que aparece
				{
					$array_key = preg_split("/-/", $key);
					$index = $array_key[1];
					$cantidad = $value;
					$id_medicamento = $_POST["id-".$index];

					array_push($medicamentos, array("id" => $id_medicamento, "cantidad" => $cantidad));
				}
			}

			$id_estatus_receta = isset($_POST["receta-completa"]) ? 2 : 1; // COMPLETA o PENDIENTE

			// INSERTAR LA NUEVA RECETA
			$insert = $pdo->prepare("INSERT INTO recetas VALUES (NULL, :folio, :id_medico, :id_trabajador, :id_beneficiario,
				:id_estatus_receta, :fechahora, (SELECT NOW()), :id_usuario, 0)");
			$success = $insert->execute(array("folio" => $folio, "id_medico" => $id_medico, "id_trabajador" => $id_trabajador, "id_beneficiario" => $id_beneficiario,
				"id_estatus_receta" => $id_estatus_receta, "fechahora" => $fechahora_elaboracion, ":id_usuario" => $id_usuario));

			// SI LA INCERSION DE LA NUEVA RECETA ES EXITOSA, ENTRA AQUÍ.
			if ($success)
			{
				$id_receta = $pdo->lastInsertId();

				// INSERTAR HISTORIAL DE RECETAS
				$insert = $pdo->query("INSERT INTO historial_ediciones_recetas VALUES (NULL, $id_receta, '$folio', $id_medico, $id_trabajador, $id_beneficiario, $id_estatus_receta, '$fechahora_elaboracion', (SELECT NOW()), $id_usuario)");

				// INSERTAR MEDICAMENTOS
				for ($i = 0; $i < count($medicamentos); $i++)
				{
					$id_medicamento = $medicamentos[$i]["id"];
					$cantidad = $medicamentos[$i]["cantidad"];

					// AMARRAR EL MEDICAMENTO CON LA RECETA
					$insert = $pdo->query("INSERT INTO medicamentos_recetas VALUES (NULL, $id_receta, $id_medicamento, $cantidad, (SELECT NOW()), $id_usuario, 0)");

					if (!$insert)
					{
						// var_dump($pdo->errorInfo());
						header("Location: ../web/recetas.php?e=2");
						exit;
					}

					// HACER LA RESTA AL INVENTARIO
					$update = $pdo->query("UPDATE medicamentos SET cantidad = (cantidad - $cantidad) WHERE id_medicamento = $id_medicamento");
					
					if (!$update)
					{
						// var_dump($pdo->errorInfo());
						header("Location: ../web/recetas.php?e=1");
						exit;
					}

					// REGISTRAR EL MOVIMIENTO
					$insert = $pdo->query("INSERT INTO movimientos_medicamentos VALUES (NULL, $id_medicamento, 0, 1, $cantidad, '', '$fecha_elaboracion', (SELECT NOW()), $id_usuario, 0)"); // SALIDA DE MEDICAMENTO POR RECETA

					if (!$insert)
					{
						// var_dump($pdo->errorInfo());
						header("Location: ../web/recetas.php?e=1");
						exit;
					}
				}

				// var_dump($pdo->errorInfo());
				header("Location: ../web/recetas.php?e=0");
				exit;
			}
			else
			{
				// var_dump("INSERT INTO recetas VALUES (NULL, :folio, :id_medico, :id_trabajador, :id_estatus_receta, (SELECT NOW()), :id_usuario, 0)");
				header("Location: ../web/recetas.php?e=1");
				exit;
			}
		break;

		case "editar-receta":
			if (!isset($_SESSION["usuario"]) || $_SESSION["usuario"]["id"] === "0")
			{
				header("Location: ../index.php?e=3");
				exit;
			}

			$id_receta = $_POST["id"];
			$id_usuario = $_SESSION["usuario"]["id"];
			$fechahora = $_POST["fechahora-elaboracion"];
			$folio = $_POST["folio"];
			$id_medico = $_POST["id-medico"];
			$id_trabajador = $_POST["id-trabajador"];
			$id_beneficiario = $_POST["beneficiario"];
			//$es_apoyo = isset($_POST["trabajador-apoyo"]) ? 1 : 0;
			//$nombre_apoyo = mb_strtoupper($_POST["trabajador"], "UTF-8");

			// DAR FORMATO A LA FECHAHORA PARA MYSQL
			$fh = preg_split("/\//", $fechahora, -1); // dd/mm/yyyy hh:mm:ss
			$day = $fh[0];
			$month = $fh[1];
			$year_time = $fh[2];
			$year_time_split = preg_split("/\s/", $year_time, -1);
			$year = $year_time_split[0];
			$time = $year_time_split[1];

			$fecha_elaboracion = $year . "-" . $month . "-" . $day;
			$fechahora_elaboracion = $year . "-" . $month . "-" . $day . " " . $time;

			$query = $pdo->query("SELECT id_medicamento_receta AS id, cantidad, id_medicamento, 0 AS existe FROM medicamentos_recetas WHERE id_receta = $id_receta AND borrado = 0");
			$medicamentos_recetas = $query->fetchAll(PDO::FETCH_ASSOC);

			foreach ($_POST as $key => $value)
			{
				if (stripos($key, "cantidad-") === 0)
				{
					$array_key = preg_split("/-/", $key);
					$index = $array_key[1];
					$cantidad = $value;
					$id_medicamento = $_POST["id-".$index];
					$id_medicamento_receta = isset($_POST["id-mr-".$index]) ? $_POST["id-mr-".$index] : 0;

					if (!$id_medicamento_receta)
					{
						$insert = $pdo->query("INSERT INTO medicamentos_recetas VALUES (NULL, $id_receta, $id_medicamento, $cantidad, (SELECT NOW()), $id_usuario, 0)");

						if (!$insert)
						{
							// var_dump($pdo->errorInfo());
							header("Location: ../web/recetas.php?e=2");
							exit;
						}

						$update = $pdo->query("UPDATE medicamentos SET cantidad = (cantidad - $cantidad) WHERE id_medicamento = $id_medicamento");

						if (!$update)
						{
							// var_dump($pdo->errorInfo());
							header("Location: ../web/recetas.php?e=1");
							exit;
						}

						$insert = $pdo->query("INSERT INTO movimientos_medicamentos VALUES (NULL, $id_medicamento, 0, 1, $cantidad, '', '$fecha_elaboracion', (SELECT NOW()), $id_usuario, 0)"); // SALIDA DE MEDICAMENTO POR RECETA

						if (!$insert)
						{
							// var_dump($pdo->errorInfo());
							header("Location: ../web/recetas.php?e=1");
							exit;
						}
					}
					else
					{
						for ($i = 0; $i < count($medicamentos_recetas); $i++)
						{
							if ($id_medicamento_receta === $medicamentos_recetas[$i]["id"])
							{
								if ($cantidad !== $medicamentos_recetas[$i]["cantidad"])
								{
									$diferencia_cantidad = ( (int) $cantidad ) - ( (int) $medicamentos_recetas[$i]["cantidad"] );
									if ($diferencia_cantidad > 0) // La cantidad nueva es mayor a la actual, entonces quitale la diferencia al inventario
									{
										$query = $pdo->query("SELECT cantidad FROM medicamentos WHERE id_medicamento = $id_medicamento");
										$cantidad_en_inventario = (int) $query->fetchColumn();
										if ((int)$cantidad > $cantidad_en_inventario)
										{
											header("Location: ../web/recetas.php?e=3");
											exit;
										}

										$update = $pdo->query("UPDATE medicamentos SET cantidad = (cantidad - $diferencia_cantidad) WHERE id_medicamento = $id_medicamento");

										if (!$update)
										{
											// var_dump($pdo->errorInfo());
											header("Location: ../web/recetas.php?e=1");
											exit;
										}

										$insert = $pdo->query("INSERT INTO movimientos_medicamentos VALUES (NULL, $id_medicamento, 0, 6, $diferencia_cantidad, 'MODIFICACION EN LA CANTIDAD DEL MEDICAMENTO EN LA RECETA', '$fecha_elaboracion', (SELECT NOW()), $id_usuario, 0)"); // SALIDA POR ERROR DE MEDICAMENTO POR RECETA

										if (!$insert)
										{
											// var_dump($pdo->errorInfo());
											header("Location: ../web/recetas.php?e=1");
											exit;
										}
									}
									else if ($diferencia_cantidad < 0) // La cantidad nueva es mayor a la actual, entonces agregale la diferencia al inventario
									{
										$diferencia_cantidad *= -1;
										$update = $pdo->query("UPDATE medicamentos SET cantidad = (cantidad + $diferencia_cantidad) WHERE id_medicamento = $id_medicamento");
										
										if (!$update)
										{
											// var_dump($pdo->errorInfo());
											header("Location: ../web/recetas.php?e=1");
											exit;
										}

										$insert = $pdo->query("INSERT INTO movimientos_medicamentos VALUES (NULL, $id_medicamento, 1, 6, $diferencia_cantidad, 'MODIFICACION EN LA CANTIDAD DEL MEDICAMENTO EN LA RECETA', '$fecha_elaboracion', (SELECT NOW()), $id_usuario, 0)"); // ENTRADA POR ERROR DE MEDICAMENTO POR RECETA

										if (!$insert)
										{
											// var_dump($pdo->errorInfo());
											header("Location: ../web/recetas.php?e=1");
											exit;
										}
									}

									$update = $pdo->query("UPDATE medicamentos_recetas SET cantidad = $cantidad WHERE id_medicamento_receta = $id_medicamento_receta");
									if (!$update)
									{
										// var_dump($pdo->errorInfo());
										header("Location: ../web/recetas.php?e=1");
										exit;
									}
								}

								$medicamentos_recetas[$i]["existe"] = true;
								break 1;
							}
						}
					}
				}
			}

			// var_dump($medicamentos_recetas); exit;

			// ENCONTRAR LOS QUE FUERON BORRADOS
			for ($i = 0; $i < count($medicamentos_recetas); $i++)
			{
				if ($medicamentos_recetas[$i]["existe"])
				{
					continue;
				}

				$id_medicamento_receta = $medicamentos_recetas[$i]["id"];
				$update = $pdo->query("UPDATE medicamentos_recetas SET borrado = 1 WHERE id_medicamento_receta = $id_medicamento_receta");
				if (!$update)
				{
					// var_dump($pdo->errorInfo());
					header("Location: ../web/recetas.php?e=1");
					exit;
				}

				$id_medicamento = $medicamentos_recetas[$i]["id_medicamento"];
				$cantidad = (int) $medicamentos_recetas[$i]["cantidad"];
				$update = $pdo->query("UPDATE medicamentos SET cantidad = cantidad + $cantidad WHERE id_medicamento = $id_medicamento");
				if (!$update)
				{
					// var_dump($pdo->errorInfo());
					header("Location: ../web/recetas.php?e=1");
					exit;
				}

				$insert = $pdo->query("INSERT INTO movimientos_medicamentos VALUES (NULL, $id_medicamento, 1, 6, $cantidad, 'ELIMINACION DEL MEDICAMENTO EN LA RECETA', '$fecha_elaboracion', (SELECT NOW()), $id_usuario, 0)"); // ENTRADA POR ERROR DE MEDICAMENTO POR RECETA

				if (!$insert)
				{
					// var_dump($pdo->errorInfo());
					header("Location: ../web/recetas.php?e=1");
					exit;
				}
			}

			$id_estatus_receta = isset($_POST["receta-completa"]) ? 2 : 1; // COMPLETA o PENDIENTE

			$update = $pdo->query("UPDATE recetas SET folio = '$folio', id_medico = $id_medico, id_trabajador = $id_trabajador, id_beneficiario = $id_beneficiario,
				id_estatus_receta = $id_estatus_receta, fecha_elaboracion = '$fechahora_elaboracion' WHERE id_receta = $id_receta");

			if ($update)
			{
				// INSERTAR HISTORIAL DE RECETAS
				$insert = $pdo->query("INSERT INTO historial_ediciones_recetas VALUES (NULL, $id_receta, '$folio', $id_medico, $id_trabajador, $id_beneficiario, $id_estatus_receta, '$fechahora_elaboracion', (SELECT NOW()), $id_usuario)");

				if (!$insert)
				{
					// var_dump($pdo->errorInfo());
					header("Location: ../web/recetas.php?e=1");
					exit;
				}

				header("Location: ../web/recetas.php?e=-1");
				exit;
			}
			else
			{
				header("Location: ../web/recetas.php?e=1");
				exit;
			}
		break;

		case "obtener-receta":
			$id_receta = $_POST["id"];

			$query = $pdo->query("SELECT r.id_receta AS id, folio,
				r.id_medico AS id_medico, UPPER(m.nombre) AS medico,
				r.id_trabajador AS id_trabajador, UPPER(TRIM(b1.nombre)) AS trabajador,
				r.id_beneficiario AS id_beneficiario, UPPER(TRIM(b2.nombre)) AS beneficiario,
				DATE_FORMAT(fecha_elaboracion, '%d/%m/%Y %H:%i') AS fechahora, IF(r.id_estatus_receta=2,'true','false') AS es_completa
				FROM recetas r
				INNER JOIN medicos m ON m.id_medico = r.id_medico
				LEFT JOIN empleados_mas_beneficiarios b1 ON b1.id = r.id_trabajador
				LEFT JOIN empleados_mas_beneficiarios b2 ON b2.id = r.id_beneficiario
				INNER JOIN estatus_receta er ON er.id_estatus_receta = r.id_estatus_receta
				WHERE id_receta = $id_receta AND r.borrado = 0");
			$receta = $query->fetch(PDO::FETCH_ASSOC);

			$query = $pdo->query("SELECT mr.id_medicamento_receta AS id,
				mr.id_medicamento AS id_medicamento, UPPER(m.nombre) AS nombre,
				mr.cantidad AS cantidad, m.cantidad AS cantidad_disponible
				FROM medicamentos_recetas mr
				INNER JOIN medicamentos m ON m.id_medicamento = mr.id_medicamento
				WHERE mr.id_receta = $id_receta AND mr.borrado = 0");
			$medicamentos = $query->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode(array("status" => "OK", "resultado" => array("receta" => $receta, "medicamentos" => $medicamentos)));
		break;

		case "obtener-medicamentos-receta":
			$id_receta = $_POST["id"];

			$query = $pdo->query("SELECT m.nombre AS medicamento, mr.cantidad
				FROM medicamentos_recetas mr
				INNER JOIN medicamentos m ON m.id_medicamento = mr.id_medicamento
				WHERE mr.id_receta = $id_receta AND mr.borrado = 0");
			$resultado = $query->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode(array("status" => "OK", "resultado" => $resultado));
		break;

		case "ver-mas-receta":
			$id_receta = $_POST["id"];

			$query = $pdo->query("SELECT folio, er.id_estatus_receta AS id_estatus,
				er.nombre AS estatus, UPPER(m.nombre) AS medico,
				UPPER(TRIM(b1.nombre)) AS trabajador,
				UPPER(TRIM(b2.nombre)) AS beneficiario,
				b2.tipo AS empresa,
				DATE_FORMAT(fecha_elaboracion, '%d/%m/%Y %H:%i') AS fechahora
				FROM recetas r
				INNER JOIN medicos m ON m.id_medico = r.id_medico
				LEFT JOIN empleados_mas_beneficiarios b1 ON b1.id = r.id_trabajador
				LEFT JOIN empleados_mas_beneficiarios b2 ON b2.id = r.id_beneficiario
				INNER JOIN estatus_receta er ON er.id_estatus_receta = r.id_estatus_receta
				WHERE id_receta = $id_receta");
			$receta = $query->fetch(PDO::FETCH_ASSOC);

			$query = $pdo->query("SELECT m.nombre AS medicamento, mr.cantidad, m.cantidad AS cantidad_disponible
				FROM medicamentos_recetas mr
				INNER JOIN medicamentos m ON m.id_medicamento = mr.id_medicamento
				WHERE mr.id_receta = $id_receta AND mr.borrado = 0");
			$medicamentos = $query->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode(array("status" => "OK", "resultado" => array("receta" => $receta, "medicamentos" => $medicamentos)));
		break;

		case "obtener-beneficiarios":
			$id_trabajador = $_POST["id_trabajador"];

			$query = $pdo->query("SELECT clave_emp AS clave, tipo FROM empleados_mas_beneficiarios WHERE id = $id_trabajador");
			$empleado = $query->fetch(PDO::FETCH_ASSOC);
			$clave_empleado = str_pad($empleado["clave"], 5, '0', STR_PAD_LEFT);
			$tipo_empleado = $empleado["tipo"];

			$query = $pdo->query("SELECT id, nombre, IF(activo=0,IF(es_nomina=1 AND TIMESTAMPDIFF(DAY,fecha_nomina,CURDATE())<=30,1,0),activo) AS activo
				FROM empleados_mas_beneficiarios WHERE LPAD(clave_benef, 5, '0') = '$clave_empleado' AND tipo LIKE CONCAT('%','$tipo_empleado','%') AND borrado = 0");

			if ($query)
			{
				$beneficiarios = $query->fetchAll(PDO::FETCH_ASSOC);
				echo json_encode(array("status" => "OK", "resultado" => $beneficiarios));
			}
			else
			{
				echo json_encode(array("status" => "ERROR"));
			}
			exit;
		break;

		case "obtener-medicamento":
			$id_medicamento = $_POST["id"];

			$query = $pdo->query("SELECT nombre, id_tipo_medicamento AS tipo, minimo, maximo,
				cantidad FROM medicamentos WHERE id_medicamento = $id_medicamento");
			$resultado = $query->fetch(PDO::FETCH_ASSOC);

			echo json_encode(array("status" => "OK", "resultado" => $resultado));
		break;

		case "obtener-tipos-medicamentos":
			$query = $pdo->query("SELECT id_tipo_medicamento AS id, nombre FROM tipos_medicamentos");
			$resultado = $query->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode(array("status" => "OK", "resultado" => $resultado));
		break;

		case "agregar-medicamento":
			if (!isset($_SESSION["usuario"]) || $_SESSION["usuario"]["id"] === "0")
			{
				echo json_encode(array("status" => "TIMEOUT"));
				exit;
			}

			$id_usuario = $_SESSION["usuario"]["id"];
			$fecha_recepcion_medicamento = $_POST["medicamento-fecha"];
			$nombre_medicamento = mb_strtoupper($_POST["medicamento-nombre"], "UTF-8");
			$id_tipo_medicamento = $_POST["medicamento-tipo"];
			$minimo = $_POST["medicamento-minimo"];
			$maximo = $_POST["medicamento-maximo"];
			$cantidad = (int) $_POST["medicamento-cantidad"];
			$id_tipo_movimiento_medicamento = $_POST["medicamento-tipo-movimiento"];
			$observaciones = mb_strtoupper($_POST["medicamento-observaciones"], "UTF-8");

			// DAR FORMATO A LA FECHAHORA PARA MYSQL
			$fr = preg_split("/\//", $fecha_recepcion_medicamento, -1); // dd/mm/yyyy hh:mm:ss
			$day = $fr[0];
			$month = $fr[1];
			$year = $fr[2];

			$fecha_recepcion_medicamento = $year . "-" . $month . "-" . $day;

			$insert = $pdo->query("INSERT INTO medicamentos VALUES (NULL, '$nombre_medicamento', $id_tipo_medicamento, $minimo, $maximo, $cantidad, 0)");
			if ($insert)
			{
				$id_medicamento = $pdo->lastInsertId();
				
				$insert = $pdo->query("INSERT INTO movimientos_medicamentos VALUES (NULL, $id_medicamento, 1, $id_tipo_movimiento_medicamento, $cantidad, '$observaciones', '$fecha_recepcion_medicamento', (SELECT NOW()), $id_usuario, 0)"); // ENTRADA DE MEDICAMENTOS
				if (!$insert)
				{
					echo json_encode(array("status" => "ERROR"));
					exit;
				}

				$id_movimiento_medicamento = $pdo->lastInsertId();
				$insert = $pdo->query("INSERT INTO historial_ediciones_medicamentos VALUES (NULL, $id_medicamento, '$nombre_medicamento', $id_tipo_medicamento, $minimo, $maximo, $id_movimiento_medicamento, $id_usuario, (SELECT NOW()))");

				echo json_encode(array("status" => "OK", "resultado" => $nombre_medicamento));
				exit;
			}
			else
			{
				echo json_encode(array("status" => "ERROR"));
				exit;
			}
		break;

		case "editar-medicamento":
			if (!isset($_SESSION["usuario"]) || $_SESSION["usuario"]["id"] === "0")
			{
				echo json_encode(array("status" => "TIMEOUT"));
				exit;
			}

			$id_usuario = $_SESSION["usuario"]["id"];
			$fecha_recepcion_medicamento = $_POST["medicamento-fecha"];
			$id_medicamento = $_POST["id"];
			$nombre_medicamento = mb_strtoupper($_POST["medicamento-nombre"], "UTF-8");
			$id_tipo_medicamento = $_POST["medicamento-tipo"];
			$minimo = $_POST["medicamento-minimo"];
			$maximo = $_POST["medicamento-maximo"];
			$cantidad_nueva = (int) $_POST["medicamento-cantidad"];
			$id_tipo_movimiento_medicamento = $_POST["medicamento-tipo-movimiento"];
			$observaciones = mb_strtoupper($_POST["medicamento-observaciones"], "UTF-8");

			// DAR FORMATO A LA FECHAHORA PARA MYSQL
			$fr = preg_split("/\//", $fecha_recepcion_medicamento, -1); // dd/mm/yyyy hh:mm:ss
			$day = $fr[0];
			$month = $fr[1];
			$year = $fr[2];

			$fecha_recepcion_medicamento = $year . "-" . $month . "-" . $day;

			$query = $pdo->query("SELECT cantidad FROM medicamentos WHERE id_medicamento = $id_medicamento");
			$cantidad_actual = (int) $query->fetchColumn();

			$id_movimiento_medicamento = false;

			if ($cantidad_nueva > $cantidad_actual)
			{
				$insert = $pdo->query("INSERT INTO movimientos_medicamentos VALUES (NULL, $id_medicamento, 1, $id_tipo_movimiento_medicamento, $cantidad_nueva - $cantidad_actual, '$observaciones', '$fecha_recepcion_medicamento', (SELECT NOW()), $id_usuario, 0)"); // ENTRADA DE MEDICAMENTOS
				if (!$insert)
				{
					echo json_encode(array("status" => "ERROR"));
					exit;
				}

				$id_movimiento_medicamento = $pdo->lastInsertId();
				$insert = $pdo->query("INSERT INTO historial_ediciones_medicamentos VALUES (NULL, $id_medicamento, '$nombre_medicamento', $id_tipo_medicamento, $minimo, $maximo, $id_movimiento_medicamento, $id_usuario, (SELECT NOW()))");
			}
			else if ($cantidad_nueva < $cantidad_actual)
			{
				$insert = $pdo->query("INSERT INTO movimientos_medicamentos VALUES (NULL, $id_medicamento, 0, $id_tipo_movimiento_medicamento, $cantidad_actual - $cantidad_nueva, '$observaciones', '$fecha_recepcion_medicamento', (SELECT NOW()), $id_usuario, 0)"); // SALIDA DE MEDICAMENTOS
				if (!$insert)
				{
					echo json_encode(array("status" => "ERROR"));
					exit;
				}

				$id_movimiento_medicamento = $pdo->lastInsertId();
				$insert = $pdo->query("INSERT INTO historial_ediciones_medicamentos VALUES (NULL, $id_medicamento, '$nombre_medicamento', $id_tipo_medicamento, $minimo, $maximo, $id_movimiento_medicamento, $id_usuario, (SELECT NOW()))");
			}

			$update = $pdo->query("UPDATE medicamentos SET nombre = '$nombre_medicamento', id_tipo_medicamento = $id_tipo_medicamento, minimo = $minimo, maximo = $maximo, cantidad =  $cantidad_nueva WHERE id_medicamento = $id_medicamento");
			if ($update)
			{
				if (!$id_movimiento_medicamento)
				{
					$insert = $pdo->query("INSERT INTO historial_ediciones_medicamentos VALUES (NULL, $id_medicamento, '$nombre_medicamento', $minimo, $maximo, $id_tipo_medicamento, 0, $id_usuario, (SELECT NOW()))");
				}

				echo json_encode(array("status" => "OK", "resultado" => $nombre_medicamento));
				exit;
			}
			else
			{
				echo json_encode(array("status" => "ERROR"));
				exit;
			}
		break;

		case "editar-tipo-medicamento":
			if (!isset($_SESSION["usuario"]) || $_SESSION["usuario"]["id"] === "0")
			{
				echo json_encode(array("status" => "TIMEOUT"));
				exit;
			}

			$id_usuario = $_SESSION["usuario"]["id"];
			$id_medicamento = $_POST["id"];
			$id_tipo_medicamento = $_POST["tipo"];

			$query = $pdo->query("SELECT nombre FROM medicamentos WHERE id_medicamento = $id_medicamento");
			$nombre_medicamento = $query->fetchColumn();

			$update = $pdo->query("UPDATE medicamentos SET id_tipo_medicamento = $id_tipo_medicamento WHERE id_medicamento = $id_medicamento");
			if (!$update)
			{
				echo json_encode(array("status" => "ERROR"));
				exit;
			}

			$insert = $pdo->query("INSERT INTO historial_ediciones_medicamentos VALUES (NULL, $id_medicamento, '$nombre_medicamento', $id_tipo_medicamento, 0, $id_usuario, (SELECT NOW()))");

			$query = $pdo->query("SELECT nombre FROM tipos_medicamentos WHERE id_tipo_medicamento = $id_tipo_medicamento");
			$nombre_tipo_medicamento = $query->fetchColumn();

			echo json_encode(array("status" => "OK", "resultado" => $nombre_tipo_medicamento));
		break;

		case "obtener-tipos-movimiento-medicamento":
			$query = $pdo->query("SELECT id_tipo_movimiento_medicamento AS id, nombre FROM tipos_movimientos_medicamentos");
			$resultado = $query->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode(array("status" => "OK", "resultado" => $resultado));
		break;

		case "alta-medicamento":
			if (!isset($_SESSION["usuario"]) || $_SESSION["usuario"]["id"] === "0")
			{
				echo json_encode(array("status" => "TIMEOUT"));
				exit;
			}

			$id_usuario = $_SESSION["usuario"]["id"];
			$fecha_recepcion_medicamento = $_POST["medicamento-fecha"];
			$id_medicamento = $_POST["id"];
			$cantidad = (int) $_POST["medicamento-cantidad"];
			$id_tipo_movimiento_medicamento = $_POST["medicamento-tipo-movimiento"];
			$observaciones = mb_strtoupper($_POST["medicamento-observaciones"], "UTF-8");

			// DAR FORMATO A LA FECHAHORA PARA MYSQL
			$fr = preg_split("/\//", $fecha_recepcion_medicamento, -1); // dd/mm/yyyy hh:mm:ss
			$day = $fr[0];
			$month = $fr[1];
			$year = $fr[2];

			$fecha_recepcion_medicamento = $year . "-" . $month . "-" . $day;

			$query = $pdo->query("SELECT cantidad FROM medicamentos WHERE id_medicamento = $id_medicamento");
			$cantidad_actual = (int) $query->fetchColumn();

			$update = $pdo->query("UPDATE medicamentos SET cantidad = cantidad + $cantidad WHERE id_medicamento = $id_medicamento");
			if ($update)
			{
				$insert = $pdo->query("INSERT INTO movimientos_medicamentos VALUES (NULL, $id_medicamento, 1, $id_tipo_movimiento_medicamento, $cantidad, '$observaciones', '$fecha_recepcion_medicamento', (SELECT NOW()), $id_usuario, 0)"); // ENTRADA DE MEDICAMENTOS
				if (!$insert)
				{
					echo json_encode(array("status" => "ERROR"));
					exit;
				}

				echo json_encode(array("status" => "OK", "resultado" => $cantidad_actual + $cantidad));
			}
			else
			{
				echo json_encode(array("status" => "ERROR"));
			}
		break;

		case "baja-medicamento":
			if (!isset($_SESSION["usuario"]) || $_SESSION["usuario"]["id"] === "0")
			{
				echo json_encode(array("status" => "TIMEOUT"));
				exit;
			}

			$id_usuario = $_SESSION["usuario"]["id"];
			$fecha_recepcion_medicamento = $_POST["medicamento-fecha"];
			$id_medicamento = $_POST["id"];
			$cantidad = (int) $_POST["medicamento-cantidad"];
			$id_tipo_movimiento_medicamento = $_POST["medicamento-tipo-movimiento"];
			$observaciones = mb_strtoupper($_POST["medicamento-observaciones"], "UTF-8");

			// DAR FORMATO A LA FECHAHORA PARA MYSQL
			$fr = preg_split("/\//", $fecha_recepcion_medicamento, -1); // dd/mm/yyyy hh:mm:ss
			$day = $fr[0];
			$month = $fr[1];
			$year = $fr[2];

			$fecha_recepcion_medicamento = $year . "-" . $month . "-" . $day;

			$query = $pdo->query("SELECT cantidad FROM medicamentos WHERE id_medicamento = $id_medicamento");
			$cantidad_actual = (int) $query->fetchColumn();

			if ($cantidad > $cantidad_actual)
			{
				echo json_encode(array("status" => "ERROR")); // ERROR LA CANTIDAD INGRESADA ES MAYOR DE LA ACTUAL EN EL INVENTARIO.
				exit;
			}

			$update = $pdo->query("UPDATE medicamentos SET cantidad = cantidad - $cantidad WHERE id_medicamento = $id_medicamento");
			if ($update)
			{
				$insert = $pdo->query("INSERT INTO movimientos_medicamentos VALUES (NULL, $id_medicamento, 0, $id_tipo_movimiento_medicamento, $cantidad, '$observaciones', '$fecha_recepcion_medicamento', (SELECT NOW()), $id_usuario, 0)"); // ENTRADA DE MEDICAMENTOS
				if (!$insert)
				{
					echo json_encode(array("status" => "ERROR"));
					exit;
				}

				echo json_encode(array("status" => "OK", "resultado" => $cantidad_actual - $cantidad));
			}
			else
			{
				echo json_encode(array("status" => "ERROR"));
			}
		break;

		case "obtener-min-max":
			$query = $pdo->query("SELECT count(*) FROM medicamentos WHERE cantidad <= minimo");
			$min = $query->fetchColumn();

			$query = $pdo->query("SELECT count(*) FROM medicamentos WHERE cantidad >= maximo");
			$max = $query->fetchColumn();

			echo json_encode(array("status" => "OK", "resultado" => array("min" => $min, "max" => $max)));
		break;

		case "obtener-reporte":
			$tipo_reporte = $_POST["tipo_reporte"];
			$fecha_inicial = $_POST["fecha_inicial"];
			$fecha_termino = $_POST["fecha_termino"];
			$filtros = json_decode($_POST["filtros"]);

			// DAR FORMATO A LA FECHA PARA MYSQL
			$fi = preg_split("/\//", $fecha_inicial, -1); // dd/mm/yyyy hh:mm:ss
			$day = $fi[0];
			$month = $fi[1];
			$year = $fi[2];
			$time = "00:00:00";

			$fecha_inicial = $year . "-" . $month . "-" . $day . " " . $time;

			// DAR FORMATO A LA FECHA PARA MYSQL
			$ft = preg_split("/\//", $fecha_termino, -1); // dd/mm/yyyy hh:mm:ss
			$day = $ft[0];
			$month = $ft[1];
			$year = $ft[2];
			$time = "23:59:59";

			$fecha_termino = $year . "-" . $month . "-" . $day . " " . $time;

			$where_filtros = count($filtros) === 0 ? "1" : "";

			switch ($tipo_reporte)
			{
				case "reporte-por-secretaria":
					for ($i = 0; $i < count($filtros); $i++)
					{
						$filtro = $filtros[$i];
						$where_filtros .= "dmd.dependencia = '$filtro'";

						if ($i < (count($filtros) - 1)) {
							$where_filtros .= " OR ";
						}
					}

					$query = $pdo->query("SELECT 1 AS num, folio, emb1.nombre AS trabajador,
					emb2.nombre AS beneficiario, emb2.tipo, (SELECT nombre FROM medicos WHERE id_medico = r.id_medico) AS medico,
					DATE_FORMAT(fecha_elaboracion, '%d/%m/%Y') AS fecha,
					(SELECT GROUP_CONCAT(CONVERT(CONCAT(mr.cantidad, ' - ', m.nombre) USING utf8) ORDER BY m.cantidad SEPARATOR '<br>') FROM medicamentos m INNER JOIN medicamentos_recetas mr ON mr.id_medicamento = m.id_medicamento WHERE mr.id_receta = r.id_receta) AS medicamento,
					(SELECT nombre FROM estatus_receta  WHERE id_estatus_receta = r.id_estatus_receta) AS estatus
					FROM recetas r INNER JOIN empleados_mas_beneficiarios emb1 ON emb1.id = r.id_trabajador
					INNER JOIN empleados_mas_beneficiarios emb2 ON emb2.id = r.id_beneficiario
					INNER JOIN deptos_mas_depends dmd ON dmd.iddependencia = emb2.iddependencia
					WHERE ( $where_filtros ) AND r.fecha_elaboracion between '$fecha_inicial' AND '$fecha_termino' AND emb2.es_apoyo = 0 GROUP BY r.id_receta ORDER BY fecha_elaboracion, trabajador, beneficiario");
					
					$columns_index = 0;
				break;

				case "reporte-por-departamento":
					for ($i = 0; $i < count($filtros); $i++)
					{
						$filtro = $filtros[$i];
						$where_filtros .= "dmd.departamento = '$filtro'";

						if ($i < (count($filtros) - 1)) {
							$where_filtros .= " OR ";
						}
					}

					$query = $pdo->query("SELECT 1 AS num, folio, emb1.nombre AS trabajador,
					emb2.nombre AS beneficiario, emb2.tipo, (SELECT nombre FROM medicos WHERE id_medico = r.id_medico) AS medico,
					DATE_FORMAT(fecha_elaboracion, '%d/%m/%Y') AS fecha,
					(SELECT GROUP_CONCAT(CONVERT(CONCAT(mr.cantidad, ' - ', m.nombre) USING utf8) ORDER BY m.cantidad SEPARATOR '<br>') FROM medicamentos m INNER JOIN medicamentos_recetas mr ON mr.id_medicamento = m.id_medicamento WHERE mr.id_receta = r.id_receta) AS medicamento,
					(SELECT nombre FROM estatus_receta  WHERE id_estatus_receta = r.id_estatus_receta) AS estatus
					FROM recetas r INNER JOIN empleados_mas_beneficiarios emb1 ON emb1.id = r.id_trabajador
					INNER JOIN empleados_mas_beneficiarios emb2 ON emb2.id = r.id_beneficiario
					INNER JOIN deptos_mas_depends dmd ON dmd.iddependencia = emb2.iddependencia
					WHERE ( $where_filtros ) AND r.fecha_elaboracion between '$fecha_inicial' AND '$fecha_termino' AND emb2.es_apoyo = 0 GROUP BY r.id_receta ORDER BY fecha_elaboracion, trabajador, beneficiario");
					
					$columns_index = 0;
				break;

				case "reporte-por-empresa":
					for ($i = 0; $i < count($filtros); $i++)
					{
						$filtro = $filtros[$i];
						$where_filtros .= "emb2.tipo = '$filtro'";

						if ($i < (count($filtros) - 1)) {
							$where_filtros .= " OR ";
						}
					}

					$query = $pdo->query("SELECT 1 AS num, folio, emb1.nombre AS trabajador,
					emb2.nombre AS beneficiario, emb2.tipo, (SELECT nombre FROM medicos WHERE id_medico = r.id_medico) AS medico,
					DATE_FORMAT(fecha_elaboracion, '%d/%m/%Y') AS fecha,
					(SELECT GROUP_CONCAT(CONVERT(CONCAT(mr.cantidad, ' - ', m.nombre) USING utf8) ORDER BY m.cantidad SEPARATOR '<br>') FROM medicamentos m INNER JOIN medicamentos_recetas mr ON mr.id_medicamento = m.id_medicamento WHERE mr.id_receta = r.id_receta) AS medicamento,
					(SELECT nombre FROM estatus_receta  WHERE id_estatus_receta = r.id_estatus_receta) AS estatus
					FROM recetas r INNER JOIN empleados_mas_beneficiarios emb1 ON emb1.id = r.id_trabajador
					INNER JOIN empleados_mas_beneficiarios emb2 ON emb2.id = r.id_beneficiario
					WHERE ( $where_filtros ) AND r.fecha_elaboracion between '$fecha_inicial' AND '$fecha_termino' ORDER BY fecha_elaboracion, trabajador, beneficiario");

					$columns_index = 0;
				break;

				case "reporte-por-trabajador":
					for ($i = 0; $i < count($filtros); $i++)
					{
						$filtro = $filtros[$i];
						$where_filtros .= "emb2.nombre = '$filtro'";

						if ($i < (count($filtros) - 1)) {
							$where_filtros .= " OR ";
						}
					}

					$query = $pdo->query("SELECT 1 AS num, folio, emb1.nombre AS trabajador,
					emb2.nombre AS beneficiario, emb2.tipo, (SELECT nombre FROM medicos WHERE id_medico = r.id_medico) AS medico,
					DATE_FORMAT(fecha_elaboracion, '%d/%m/%Y') AS fecha,
					(SELECT GROUP_CONCAT(CONVERT(CONCAT(mr.cantidad, ' - ', m.nombre) USING utf8) ORDER BY m.cantidad SEPARATOR '<br>') FROM medicamentos m INNER JOIN medicamentos_recetas mr ON mr.id_medicamento = m.id_medicamento WHERE mr.id_receta = r.id_receta) AS medicamento,
					(SELECT nombre FROM estatus_receta  WHERE id_estatus_receta = r.id_estatus_receta) AS estatus
					FROM recetas r INNER JOIN empleados_mas_beneficiarios emb1 ON emb1.id = r.id_trabajador
					INNER JOIN empleados_mas_beneficiarios emb2 ON emb2.id = r.id_beneficiario
					WHERE ( $where_filtros ) AND r.fecha_elaboracion between '$fecha_inicial' AND '$fecha_termino' AND es_nomina = 1 ORDER BY fecha_elaboracion, trabajador, beneficiario");

					$columns_index = 0;
				break;

				case "reporte-por-beneficiario":
					for ($i = 0; $i < count($filtros); $i++)
					{
						$filtro = $filtros[$i];
						$where_filtros .= "emb2.nombre = '$filtro'";

						if ($i < (count($filtros) - 1)) {
							$where_filtros .= " OR ";
						}
					}

					$query = $pdo->query("SELECT 1 AS num, folio, emb1.nombre AS trabajador,
					emb2.nombre AS beneficiario, emb2.tipo, (SELECT nombre FROM medicos WHERE id_medico = r.id_medico) AS medico,
					DATE_FORMAT(fecha_elaboracion, '%d/%m/%Y') AS fecha,
					(SELECT GROUP_CONCAT(CONVERT(CONCAT(mr.cantidad, ' - ', m.nombre) USING utf8) ORDER BY m.cantidad SEPARATOR '<br>') FROM medicamentos m INNER JOIN medicamentos_recetas mr ON mr.id_medicamento = m.id_medicamento WHERE mr.id_receta = r.id_receta) AS medicamento,
					(SELECT nombre FROM estatus_receta  WHERE id_estatus_receta = r.id_estatus_receta) AS estatus
					FROM recetas r INNER JOIN empleados_mas_beneficiarios emb1 ON emb1.id = r.id_trabajador
					INNER JOIN empleados_mas_beneficiarios emb2 ON emb2.id = r.id_beneficiario
					WHERE ( $where_filtros ) AND r.fecha_elaboracion between '$fecha_inicial' AND '$fecha_termino' AND es_nomina = 0 AND es_apoyo = 0 ORDER BY fecha_elaboracion, trabajador, beneficiario");

					$columns_index = 0;
				break;

				case "reporte-por-apoyo":
					for ($i = 0; $i < count($filtros); $i++)
					{
						$filtro = $filtros[$i];
						$where_filtros .= "emb2.nombre = '$filtro'";

						if ($i < (count($filtros) - 1)) {
							$where_filtros .= " OR ";
						}
					}

					$query = $pdo->query("SELECT 1 AS num, folio, emb1.nombre AS trabajador,
					emb2.nombre AS beneficiario, emb2.tipo, (SELECT nombre FROM medicos WHERE id_medico = r.id_medico) AS medico,
					DATE_FORMAT(fecha_elaboracion, '%d/%m/%Y') AS fecha,
					(SELECT GROUP_CONCAT(CONVERT(CONCAT(mr.cantidad, ' - ', m.nombre) USING utf8) ORDER BY m.cantidad SEPARATOR '<br>') FROM medicamentos m INNER JOIN medicamentos_recetas mr ON mr.id_medicamento = m.id_medicamento WHERE mr.id_receta = r.id_receta) AS medicamento,
					(SELECT nombre FROM estatus_receta  WHERE id_estatus_receta = r.id_estatus_receta) AS estatus
					FROM recetas r INNER JOIN empleados_mas_beneficiarios emb1 ON emb1.id = r.id_trabajador
					INNER JOIN empleados_mas_beneficiarios emb2 ON emb2.id = r.id_beneficiario
					WHERE ( $where_filtros ) AND r.fecha_elaboracion between '$fecha_inicial' AND '$fecha_termino' AND es_apoyo = 1 ORDER BY fecha_elaboracion, trabajador, beneficiario");

					$columns_index = 0;
				break;

				case "reporte-por-medico":
					for ($i = 0; $i < count($filtros); $i++)
					{
						$filtro = $filtros[$i];
						$where_filtros .= "m.nombre = '$filtro'";

						if ($i < (count($filtros) - 1)) {
							$where_filtros .= " OR ";
						}
					}

					$query = $pdo->query("SELECT 1 AS num, folio, emb1.nombre AS trabajador,
					emb2.nombre AS beneficiario, emb2.tipo, (SELECT nombre FROM medicos WHERE id_medico = r.id_medico) AS medico,
					DATE_FORMAT(fecha_elaboracion, '%d/%m/%Y') AS fecha,
					(SELECT GROUP_CONCAT(CONVERT(CONCAT(mr.cantidad, ' - ', m.nombre) USING utf8) ORDER BY m.cantidad SEPARATOR '<br>') FROM medicamentos m INNER JOIN medicamentos_recetas mr ON mr.id_medicamento = m.id_medicamento WHERE mr.id_receta = r.id_receta) AS medicamento,
					(SELECT nombre FROM estatus_receta  WHERE id_estatus_receta = r.id_estatus_receta) AS estatus
					FROM recetas r INNER JOIN empleados_mas_beneficiarios emb1 ON emb1.id = r.id_trabajador
					INNER JOIN empleados_mas_beneficiarios emb2 ON emb2.id = r.id_beneficiario
					INNER JOIN medicos m ON m.id_medico = r.id_medico
					WHERE ( $where_filtros ) AND r.fecha_elaboracion between '$fecha_inicial' AND '$fecha_termino' ORDER BY fecha_elaboracion, trabajador, beneficiario");

					$columns_index = 0;
				break;

				case "reporte-por-min-max":
					for ($i = 0; $i < count($filtros); $i++)
					{
						$filtro = $filtros[$i];
						$where_filtros .= "m.nombre = '$filtro'";

						if ($i < (count($filtros) - 1)) {
							$where_filtros .= " OR ";
						}
					}

					$query = $pdo->query("SELECT 1 AS num, m.nombre AS medicamento, tm.nombre AS tipo_medicamento, minimo, maximo, cantidad
					FROM medicamentos m INNER JOIN tipos_medicamentos tm ON tm.id_tipo_medicamento = m.id_tipo_medicamento
					WHERE ( $where_filtros ) AND ( cantidad <= minimo OR cantidad >= maximo ) ORDER BY m.nombre");

					$columns_index = 1;
				break;

				case "reporte-por-ajuste-inventario":
					for ($i = 0; $i < count($filtros); $i++)
					{
						$filtro = $filtros[$i];
						$where_filtros .= "m.nombre = '$filtro'";

						if ($i < (count($filtros) - 1)) {
							$where_filtros .= " OR ";
						}
					}

					$query = $pdo->query("SELECT 1 AS num, m.nombre AS medicamento, tm.nombre AS tipo_medicamento, cantidad, '' AS cantidadFisica
					FROM medicamentos m INNER JOIN tipos_medicamentos tm ON tm.id_tipo_medicamento = m.id_tipo_medicamento
					WHERE ( $where_filtros ) ORDER BY m.nombre");

					$columns_index = 2;
				break;
			}

			$columns = array(
				array(
					array("title"=>"#", "width"=>"3%"),
					array("title"=>"Folio", "width"=>"7.5%"),
					array("title"=>"Trabajador", "width"=>"15%"),
					array("title"=>"Beneficiario", "width"=>"15%"),
					array("title"=>"Empresa", "width"=>"9%"),
					array("title"=>"Médico", "width"=>"15%"),
					array("title"=>"Fecha de Elaboración", "width"=>"7%"),
					array("title"=>"Medicamento", "width"=>"22.5%"),
					array("title"=>"Estatus", "width"=>"6%")
				),
				array(
					array("title"=>"#", "width"=>"5%"),
					array("title"=>"Medicamento", "width"=>"50%"),
					array("title"=>"Tipo de Medicamento", "width"=>"15%"),
					array("title"=>"Mín.", "width"=>"10%", "className" => "text-right"),
					array("title"=>"Máx.", "width"=>"10%", "className" => "text-right"),
					array("title"=>"Cantidad", "width"=>"10%", "className" => "text-right")
				),
				array(
					array("title"=>"#", "width"=>"5%"),
					array("title"=>"Medicamento", "width"=>"70%"),
					array("title"=>"Tipo de Medicamento", "width"=>"15%"),
					array("title"=>"Cantidad Sistema", "width"=>"5%", "className" => "text-center"),
					array("title"=>"Cantidad Física", "width"=>"5%", "className" => "text-center")
				)
			);

			$data = $query->fetchAll(PDO::FETCH_NUM);
			echo json_encode(array("status" => "OK", "resultado" => array("data" => $data, "columns" => $columns[$columns_index])));
		break;

		case "obtener-reporte-medicamentos":
			$fecha_inicial = $_POST["fecha_inicial"];
			$fecha_termino = $_POST["fecha_termino"];

			// DAR FORMATO A LA FECHA PARA MYSQL
			$fi = preg_split("/\//", $fecha_inicial, -1); // dd/mm/yyyy hh:mm:ss
			$day = $fi[0];
			$month = $fi[1];
			$year = $fi[2];
			$time = "00:00:00";

			$fecha_inicial = $year . "-" . $month . "-" . $day . " " . $time;

			// DAR FORMATO A LA FECHA PARA MYSQL
			$ft = preg_split("/\//", $fecha_termino, -1); // dd/mm/yyyy hh:mm:ss
			$day = $ft[0];
			$month = $ft[1];
			$year = $ft[2];
			$time = "23:59:59";

			$fecha_termino = $year . "-" . $month . "-" . $day . " " . $time;

			$query = $pdo->query("SELECT 1 AS num, m.nombre AS medicamento,
				SUM(mm.cantidad) AS cantidad, IF(mm.entrada_salida=1,'ENTRADA', 'SALIDA') AS entrada_salida, tmm.nombre AS movimiento
				FROM movimientos_medicamentos mm
				INNER JOIN medicamentos m ON m.id_medicamento = mm.id_medicamento
				INNER JOIN tipos_movimientos_medicamentos tmm ON tmm.id_tipo_movimiento_medicamento = mm.id_tipo_movimiento_medicamento
				WHERE mm.borrado = 0 AND mm.fecha_recepcion between '$fecha_inicial' AND '$fecha_termino'
				GROUP BY mm.id_medicamento, mm.entrada_salida, tmm.id_tipo_movimiento_medicamento
				ORDER BY m.nombre, mm.entrada_salida");
			$data = $query->fetchAll(PDO::FETCH_NUM);

			$columns = array(
				array("title"=>"#", "width"=>"5%"),
				array("title"=>"Medicamento", "width"=>"60%"),
				array("title"=>"Cantidad", "width"=>"5%", "className" => "td-cantidad"),
				array("title"=>"Entrada o Salida", "width"=>"15%"),
				array("title"=>"Movimiento", "width"=>"15%")
			);

			echo json_encode(array("status" => "OK", "resultado" => array("data" => $data, "columns" => $columns)));
		break;

		case "obtener-reporte-medicos":
			$fecha_inicial = $_POST["fecha_inicial"];
			$fecha_termino = $_POST["fecha_termino"];

			// DAR FORMATO A LA FECHA PARA MYSQL
			$fi = preg_split("/\//", $fecha_inicial, -1); // dd/mm/yyyy hh:mm:ss
			$day = $fi[0];
			$month = $fi[1];
			$year = $fi[2];
			$time = "00:00:00";

			$fecha_inicial = $year . "-" . $month . "-" . $day . " " . $time;

			// DAR FORMATO A LA FECHA PARA MYSQL
			$ft = preg_split("/\//", $fecha_termino, -1); // dd/mm/yyyy hh:mm:ss
			$day = $ft[0];
			$month = $ft[1];
			$year = $ft[2];
			$time = "23:59:59";

			$fecha_termino = $year . "-" . $month . "-" . $day . " " . $time;

			$query = $pdo->query("SELECT med.nombre AS medico, SUM(mr.cantidad) AS cantidad
				FROM recetas r
				INNER JOIN medicos med ON med.id_medico = r.id_medico
				INNER JOIN medicamentos_recetas mr ON mr.id_receta = r.id_receta
				WHERE r.borrado = 0 AND mr.borrado = 0 AND r.fecha_elaboracion between '$fecha_inicial' AND '$fecha_termino'
				GROUP BY r.id_medico
				ORDER BY cantidad DESC");
			$resultado = $query->fetchAll(PDO::FETCH_ASSOC);
		break;

		case "obtener-reporte-trabajadores":
			$fecha_inicial = $_POST["fecha_inicial"];
			$fecha_termino = $_POST["fecha_termino"];

			// DAR FORMATO A LA FECHA PARA MYSQL
			$fi = preg_split("/\//", $fecha_inicial, -1); // dd/mm/yyyy hh:mm:ss
			$day = $fi[0];
			$month = $fi[1];
			$year = $fi[2];
			$time = "00:00:00";

			$fecha_inicial = $year . "-" . $month . "-" . $day . " " . $time;

			// DAR FORMATO A LA FECHA PARA MYSQL
			$ft = preg_split("/\//", $fecha_termino, -1); // dd/mm/yyyy hh:mm:ss
			$day = $ft[0];
			$month = $ft[1];
			$year = $ft[2];
			$time = "23:59:59";

			$fecha_termino = $year . "-" . $month . "-" . $day . " " . $time;

			$query = $pdo->query("SELECT IF(r.id_trabajador = 0, r.nombre_apoyo, UPPER(nom.nombre)) AS trabajador,				
				UPPER(TRIM(b1.nombre_completo))) AS trabajador,
				UPPER(TRIM(b2.nombre_completo))) AS beneficiario,
				b2.tipo_beneficiario AS empresa,
				SUM(mr.cantidad) AS cantidad
				FROM recetas r
				LEFT JOIN beneficiarios b1 ON b1.id_beneficiario = r.id_trabajador
				LEFT JOIN beneficiarios b2 ON b2.id_beneficiario = r.id_beneficiario
				INNER JOIN medicamentos_recetas mr ON mr.id_receta = r.id_receta
				WHERE r.borrado = 0 AND mr.borrado = 0 AND r.fecha_elaboracion between '$fecha_inicial' AND '$fecha_termino'
				GROUP BY trabajador
				ORDER BY cantidad DESC");
			$resultado = $query->fetchAll(PDO::FETCH_ASSOC);
		break;

		case "obtener-reporte-historial-medicamentos":
			$fecha_inicial = $_POST["fecha_inicial"];
			$fecha_termino = $_POST["fecha_termino"];

			// DAR FORMATO A LA FECHA PARA MYSQL
			$fi = preg_split("/\//", $fecha_inicial, -1); // dd/mm/yyyy hh:mm:ss
			$day = $fi[0];
			$month = $fi[1];
			$year = $fi[2];
			$time = "00:00:00";

			$fecha_inicial = $year . "-" . $month . "-" . $day . " " . $time;

			// DAR FORMATO A LA FECHA PARA MYSQL
			$ft = preg_split("/\//", $fecha_termino, -1); // dd/mm/yyyy hh:mm:ss
			$day = $ft[0];
			$month = $ft[1];
			$year = $ft[2];
			$time = "23:59:59";

			$fecha_termino = $year . "-" . $month . "-" . $day . " " . $time;

			$query = $pdo->query("SELECT m.nombre AS medicamento, IF(mm.entrada_salida=1,'ENTRADA', 'SALIDA') AS entrada_salida,
				mm.cantidad AS cantidad, tmm.nombre AS movimiento, DATE_FORMAT(mm.fecha_captura, '%d/%m/%Y %H:%i') AS fechahora
				FROM movimientos_medicamentos mm
				INNER JOIN medicamentos m ON m.id_medicamento = mm.id_medicamento
				INNER JOIN tipos_movimientos_medicamentos tmm ON tmm.id_tipo_movimiento_medicamento = mm.id_tipo_movimiento_medicamento
				WHERE mm.fecha_captura between '$fecha_inicial' AND '$fecha_termino'
				ORDER BY mm.fecha_captura DESC");
			$resultado = $query->fetchAll(PDO::FETCH_ASSOC);
		break;
	}
?>