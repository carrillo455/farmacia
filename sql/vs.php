<?php 
	$pdo = new PDO("pgsql:dbname=farmacia;host=localhost;user=postgres;password=qweqwe");
	$query = $pdo->query("SELECT iif.medicamento_farmacia_1 AS medicamento, (SELECT SUM(cantidad) FROM inventario_farmacia_recetas WHERE medicamento = iif.medicamento_farmacia_1 GROUP BY medicamento) AS cantidad
		FROM inventario_inicial_vs_farmacia iif GROUP BY medicamento_farmacia_1 HAVING medicamento_farmacia_1 IS NOT NULL ORDER BY medicamento");
	$farmacia = $query->fetchAll(PDO::FETCH_ASSOC);

	//var_dump($farmacia);

	$query = $pdo->query("SELECT medicamento_inicial AS medicamento, cantidad AS cantidad, medicamento_farmacia_1 AS medicamento_farmacia FROM inventario_inicial_vs_farmacia");
	$inv_inicial = $query->fetchAll(PDO::FETCH_ASSOC);

	for ($i = 0; $i < count($inv_inicial); $i++)
	{
		$medicamento_inicial = $inv_inicial[$i]["medicamento"];
		$cantidad_inicial = $inv_inicial[$i]["cantidad"];
		$medicamento_farmacia = $inv_inicial[$i]["medicamento_farmacia"];

		for ($j = 0; $j < count($farmacia); $j++)
		{
			$key = array_search($medicamento_farmacia, $farmacia[$j]);
			$inv_inicial[$i]["cantidad_farmacia"] = 0;

			if ($key)
			{
				$cantidad_farmacia = $farmacia[$j]["cantidad"];
				// $medicamento_farmacia = $farmacia[$j][$key];
				// $inv_inicial[$i]["cantidad"] = ($cantidad_inicial - $cantidad_farmacia) < 0 ? 0 : ($cantidad_inicial - $cantidad_farmacia);
				// $farmacia[$j]["cantidad"] = ($cantidad_farmacia - $cantidad_inicial) < 0 ? 0 : ($cantidad_farmacia - $cantidad_inicial);
				$inv_inicial[$i]["cantidad"] = ($cantidad_inicial - $cantidad_farmacia);
				$farmacia[$j]["cantidad"] = ($cantidad_farmacia - $cantidad_inicial);
				
				$inv_inicial[$i]["cantidad_farmacia"] = $farmacia[$j]["cantidad"];

				break 1;
			}
		}
	}

	echo "<meta charset='utf-8'>";
	echo "<table border=1>";
	echo "<thead><tr><th>#</th><th>MEDICAMENTO INV. INICIAL</th><th>CANT. INV. INICIAL</th><th>MEDICAMENTO FARMACIA RECETAS</th><th>CANTIDAD FARMACIA RECETAS</th></tr></thead>";
	echo "<tbody>";

	for ($i = 0; $i < count($inv_inicial); $i++)
	{
		$medicamento_inicial = $inv_inicial[$i]["medicamento"];
		$cantidad_inicial = $inv_inicial[$i]["cantidad"];
		$medicamento_farmacia = $inv_inicial[$i]["medicamento_farmacia"];
		$cantidad_farmacia = $inv_inicial[$i]["cantidad_farmacia"];

		echo "<tr><td>".($i+1)."</td><td>".$medicamento_inicial."</td><td>".$cantidad_inicial."</td><td>".$medicamento_farmacia."</td><td>".$cantidad_farmacia."</td></tr>";
	}

	echo "</tbody></table>";
 ?>