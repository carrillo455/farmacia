<?php
	$pdo = new PDO("pgsql:dbname=farmacia;host=localhost;user=postgres;password=qweqwe");

	echo "<meta charset='utf-8'>";
	echo "<table border=1>";
	echo "<thead><tr><th>#</th><th>MEDICAMENTO INV. INICIAL</th><th>CANT. INV. INICIAL</th><th>MEDICAMENTO FARMACIA RECETAS</th><th>CANT. FARMACIA RECETAS</th></tr></thead>";
	echo "<tbody>";

	$query = $pdo->query("SELECT medicamento, SUM(cantidad) AS cantidad FROM inventario_inicial GROUP BY medicamento ORDER BY medicamento");
	$inventario_inicial = $query->fetchAll(PDO::FETCH_ASSOC);

	$query = $pdo->query("SELECT medicamento, SUM(cantidad) AS cantidad FROM inventario_farmacia_global GROUP BY medicamento ORDER BY medicamento");
	$inventario_farmacia_recetas = $query->fetchAll(PDO::FETCH_ASSOC);

	for ($i = 0;  $i < count($inventario_inicial); $i++)
	{
		$medicamento_ii = strtoupper($inventario_inicial[$i]["medicamento"]);
		$cantidad_ii = $inventario_inicial[$i]["cantidad"];

		$encontro_similitud = false;

		for ($j = 0; $j < count($inventario_farmacia_recetas); $j++)
		{
			$medicamento_ifr = strtoupper($inventario_farmacia_recetas[$j]["medicamento"]);
			$cantidad_ifr = $inventario_farmacia_recetas[$j]["cantidad"];

			similar_text($medicamento_ii, $medicamento_ifr, $porcentaje_similitud);
			if ($porcentaje_similitud > 75)
			{
				$encontro_similitud = true;
				break 1;
			}
		}

		if (!$encontro_similitud)
		{
			$medicamento_ifr = "";
			$cantidad_ifr = "";
		}

		echo "<tr><td>".($i+1)."</td><td>".$medicamento_ii."</td><td>".$cantidad_ii."</td><td>".$medicamento_ifr."</td><td>".$cantidad_ifr."</td></tr>";
	}

	echo "</tbody></table>";
?>