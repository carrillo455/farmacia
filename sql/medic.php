<?php 
	$pdo = new PDO("pgsql:dbname=farmacia;host=localhost;user=postgres;password=qweqwe");
	$query = $pdo->query("SELECT id, medicamento, venta FROM medicamentos m ORDER BY medicamento");
	$medicamentos = $query->fetchAll(PDO::FETCH_ASSOC);

	$query = $pdo->query("SELECT UPPER(medicamento) AS medicamento FROM inventario_farmacia_recetas  GROUP BY medicamento ORDER BY medicamento");
	$recetas = $query->fetchAll(PDO::FETCH_ASSOC);

	echo "<meta charset='utf-8'>";
	echo "<table border=1>";
	echo "<thead><tr><th>#</th><th>MEDICAMENTO CATALOGO</th><th>MEDICAMENTO RECETA</th><th>VENTA</th></tr></thead>";
	echo "<tbody>";

	$cont = 1;

	for ($i = 0; $i < count($recetas); $i++)
	{
		$encontro_similitud = false;
		$medicamento_receta = $recetas[$i]["medicamento"];

		for ($j = 0; $j < count($medicamentos); $j++)
		{
			$medicamento_catalogo = $medicamentos[$j]["medicamento"];
			$medicamento_venta = $medicamentos[$j]["venta"];
			
			similar_text($medicamento_receta, $medicamento_catalogo, $porcentaje_similitud);
			if ($porcentaje_similitud > 75)
			{
				$encontro_similitud = true;
				break 1;
			}
		}

		if ($encontro_similitud)
		{
			echo "<tr><td>".$cont."</td><td>".$medicamento_catalogo."</td><td>".$medicamento_receta."</td><td>".$medicamento_venta."</td></tr>";
			$cont += 1;
		}
	}

	echo "</tbody></table>";
?>